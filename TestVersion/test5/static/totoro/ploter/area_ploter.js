
function load_area_plot(){
    var x_col=arguments[0]?arguments[0]:0;
    var y_col=arguments[1]?arguments[1]:0;
    var g_col=arguments[2]?arguments[2]:"";
    var title = arguments[3]?arguments[3]:"title ";
    var x_title = arguments[4]?arguments[4]:"x title";
    var y_title = arguments[5]?arguments[5]:"y title";

    $("#plot_setting_div").load("/static/totoro/ploter/area_ploter.html", function(){
        $("#title_edit").val(title);
        $("#x_title_edit").val(x_title);
        $("#y_title_edit").val(y_title);
        $("#x_col_edit").val(x_col);
        $("#y_col_edit").val(y_col);
        $("#g_col_edit").val(g_col);
    });


}

function area_plot_plot() {
  AJAX("qtype=SQL&sql_cmd=" + $("#sql_edit").val(), area_plot_handler);
}

function area_plot_handler(res){
    freshSQLTable(res);
    var data_map_list=new Array();

    var jsonObj = JSON.parse(res);
    var row = jsonObj.length;
    var col =jsonObj[0].length;
    var x_col = parseInt($("#x_col_edit").val());
    var y_col = parseInt($("#y_col_edit").val());
    var g_col = parseInt($("#g_col_edit").val());

    var data_map=new Map();

    for(var i=0; i<row; i++){
        var g_name = "";
        if(g_col>=0 && g_col<col){
          g_name = jsonObj[i][g_col];
        }
        var x = parseFloat(jsonObj[i][x_col]); 
        var y = parseFloat(jsonObj[i][y_col]);

        if(!data_map.has(g_name)){
            data_map.set(g_name, new Array());
        }
        data_map.get(g_name).push(new Array(x,y));
    }  

    data_map.forEach(function(item,key,obj){
        item.sort(function(a,b){return a[0]-b[0]})
        data_map_list.push({name:key, data:item});
    });

    $('#plotContainer').highcharts({
        chart: {
            type: 'area'
        },
        title: {
            text: $("#title_edit").val()
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            title: {
                text: $("#x_title_edit").val()
            }
        },
        yAxis: {
            title: {
                text: $("#y_title_edit").val()
            },
            min: 0
        },
        tooltip: {
            headerFormat: '<b>{series.name}</b><br>',
            pointFormat: '{point.x:%e. %b}: {point.y:.2f} m'
        },
        plotOptions: {
            spline: {
                marker: {
                    enabled: true
                }
            }
        },
        series: data_map_list
    });
}

