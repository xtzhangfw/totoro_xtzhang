
function load_pie_plot(){
    var x_col=arguments[0]?arguments[0]:0;
    var y_col=arguments[1]?arguments[1]:0;
    var title = arguments[2]?arguments[2]:"title ";

    $("#plot_setting_div").load("/static/totoro/ploter/pie_ploter.html", function(){
        $("#title_edit").val(title);
        $("#x_col_edit").val(x_col);
        $("#y_col_edit").val(y_col);
    });


}

function pie_plot_plot() {
  AJAX("qtype=SQL&sql_cmd=" + $("#sql_edit").val(), pie_plot_handler);
}

function pie_plot_handler(res){
    freshSQLTable(res);
    var data_list=new Array();
    var jsonObj = JSON.parse(res);
    var row = jsonObj.length;
    var col =jsonObj[0].length;
    var x_col = parseInt($("#x_col_edit").val());
    var y_col = parseInt($("#y_col_edit").val());

    for(var i=0; i<row; i++){
        var x = (jsonObj[i][x_col]); 
        var y = parseFloat(jsonObj[i][y_col]);
        data_list.push(new Array(x,y));
    }  

    $('#plotContainer').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false
        },
        title: {
            text: $("#title_edit").val()
        },
        tooltip: {
            headerFormat: '{series.name}<br>',
            pointFormat: '{point.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            type: 'pie',
            name: $("#title_edit").val(),
            data: data_list
        }]
    });


}

