$(function(){

  AJAX("qtype=COM&com_cmd=tableinfo", freshTableList);

  $("#table_selector").change(function(){
    var tname=$("#table_selector").val();
    AJAX("qtype=COM&com_cmd=analyser&table_name=" + tname, freshAnalyserList);
    AJAX("qtype=COM&com_cmd=tableheadinfo&table_name=" + tname, freshTableHeadInfo);
  });
  
});


function runTSQL(){
    AJAX("qtype=SQL&sql_cmd=" + $("#sql_edit").val(), freshSQLTable);
}


function freshTableHeadInfo(res){
  var jsonObj = JSON.parse(res);
  var row = jsonObj.length;
  var col =jsonObj[0].length;
  var col_name_str = "<tr>";

  for(var i=0; i<row; i++) {
    col_name_str += "<td>" + jsonObj[i][4] + "</td>";
  }
  col_name_str += "</tr>";

  var col_type_str = "<tr>";
  for(var i=0; i<row; i++) {
    col_type_str += "<td>" + jsonObj[i][5] + "</td>";
  }
  col_type_str += "</tr>";


  $("#table_head_info_table").html(col_name_str + col_type_str);

}


function freshSQLTable(res){
    var jsonObj = JSON.parse(res);
    var row = jsonObj.length;
    var col =jsonObj[0].length;
    var inHtml = "<thead><tr>";

    for(var i=0; i<col; i++){
      inHtml += "<th>Col:  " + i + "</th>";
    }
    inHtml += "</tr></thead><tbody>";

    for(var i=0; i<row; i++){
         inHtml += "<tr>"
         for(var j=0; j<col; j++){
           inHtml += "<td>" + jsonObj[i][j] + "</td>";
         }
         inHtml += "</tr>"
    }
    inHtml += "</tbody>";

    $("#sql_table").html(inHtml)

}

function freshTableList(res){
    var jsonObj = JSON.parse(res);
    var row = jsonObj.length;
    var col =jsonObj[0].length;
    $("#table_selector").empty();
    var inHtml = "";
    for(var i=0; i<row; i++){
         inHtml += ("<option value='" + jsonObj[i][2] + "'>" + jsonObj[i][2] + "</option>");
    }
    $("#table_selector").html(inHtml);
    $("#table_selector").selectpicker('refresh');
}

function freshAnalyserList(res){
    var jsonObj = JSON.parse(res);
    var row = jsonObj.length;
    var col =jsonObj[0].length;
    $("#analyser_selector").empty();
    var inHtml = "<option value='user_define'>User Define</option>";
    for(var i=0; i<row; i++){
         inHtml += ("<option value='" + jsonObj[i][1] + "'>" + jsonObj[i][1] + "</option>");
    }
    $("#analyser_selector").html(inHtml);
    $("#analyser_selector").selectpicker('refresh');

    $("#analyser_selector").change(function(){
      $("#sql_edit").val("");
      load_line_plot();

      var i=0;
      for(i=0; i<row; i++){
        if(jsonObj[i][1]==$(this).val()) break;
      }
      var sql_cmd = jsonObj[i][4];
      var plot_cmd = jsonObj[i][5];
      var plot_cmd_list = plot_cmd.split(",");
      var plot_name = plot_cmd_list[0];
      $("#sql_edit").val(sql_cmd);

      if(plot_name=="line_ploter"){
        var x_col = plot_cmd_list[1];
        var y_col = plot_cmd_list[2];
        var g_col = plot_cmd_list[3];
        var title = plot_cmd_list[4];
        var x_title = plot_cmd_list[5];
        var y_title = plot_cmd_list[6];
        load_line_plot(x_col, y_col, g_col, title, x_title, y_title);
      }else if(plot_name=="area_ploter"){
        var x_col = plot_cmd_list[1];
        var y_col = plot_cmd_list[2];
        var g_col = plot_cmd_list[3];
        var title = plot_cmd_list[4];
        var x_title = plot_cmd_list[5];
        var y_title = plot_cmd_list[6];
        load_area_plot(x_col, y_col, g_col, title, x_title, y_title);
      }else if(plot_name=="pie_ploter"){
        var x_col = plot_cmd_list[1];
        var y_col = plot_cmd_list[2];
        var title = plot_cmd_list[3];
        load_pie_plot(x_col, y_col, title);
      }else{
      }
      
    });

}

function showTSQLPanel(){
  $("#sqldiv").show()
  $("#plotdiv").hide()
  $("#sqlbt").attr("class", "active")
  $("#plotbt").attr("class", "inactive")
}

function showPlotPanel(){
  $("#sqldiv").hide()
  $("#plotdiv").show()
  $("#sqlbt").attr("class", "inactive")
  $("#plotbt").attr("class", "active")
}


function AJAX(cmd, funHandler){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function(){
        if(this.readyState==4 && this.status==200){
			      var res = this.responseText;
            funHandler(res)
        }
    };
    xhttp.open("POST","Query",true);
	  xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhttp.send(cmd);
}


