import java.net._
import java.io._
import scala.io._

object QueryClient{

  def main(args:Array[String]){
    val s = new Socket(InetAddress.getByName("localhost"), 9999)
    lazy val in = new BufferedSource(s.getInputStream()).getLines()
    val out = new PrintStream(s.getOutputStream())
    out.println("select * from d_placement")
    out.flush()

    in.foreach(println)
    s.close()
  }
}
