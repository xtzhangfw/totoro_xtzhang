package MyParser

import java.util

import MyScanner.Scanner

import scala.collection.mutable
/*
class FunItem(val FunName:String, val ParaList: List[String]){
  override def toString: String = {
    super.toString
    val para = ParaList.mkString(",")
    val ans = FunName + "(" + para + ")"
    ans
  }
}
*/

class Parser {

  def ccexpParse(scanner:Scanner):(List[Any], Scanner) = {
    val tmp = scanner.next()
    val tmp1 = tmp._2.next()
    if(tmp1._1 == "("){
      val flist = funListParse(tmp1._2, Nil)
      (tmp._1 :: flist._1, flist._2.next()._2)//skip the last )

    }else{
      (List[Any]("DEFAULT", tmp._1), tmp._2)
    }
  }

  def cexpParse(scanner:Scanner): (List[Any], Scanner) ={
    val tmp = scanner.next()
    if(tmp._1 == "("){
      val tmp1 = expParse(tmp._2,Nil)
      val tmp2 = tmp1._2.next()
      (tmp1._1, tmp2._2)
    }
    else {
      val tmp1 = ccexpParse(scanner)
      val tmp2 = tmp1._2.next()
      if(tmp2._1==">=" || tmp2._1=="<=" || tmp2._1=="!=" || tmp2._1==">" || tmp2._1=="<" || tmp2._1=="=" || tmp2._1=="==") {
        val tmp3 = ccexpParse(tmp2._2)
        (List[Any](tmp2._1, tmp1._1, tmp3._1), tmp3._2)
      }else{
        tmp1
      }
    }
  }

  def expANDParse(scanner:Scanner, cur:List[Any]): (List[Any], Scanner) ={
    if(cur==Nil){
      val tmp = cexpParse(scanner)
      expANDParse(tmp._2, tmp._1)
    }else{
      val tmp = scanner.next()
      if(tmp._1.toUpperCase == "AND"){
        val tmp1 = cexpParse(tmp._2)
        expANDParse(tmp1._2, List[Any]("AND", cur, tmp1._1))
      }
      else{
        (cur, scanner)
      }
    }
  }

  def expParse(scanner:Scanner, cur:List[Any]):(List[Any], Scanner) = {
    if(cur==Nil){
      val tmp = expANDParse(scanner, Nil)
      expParse(tmp._2, tmp._1)
    }else{
      val tmp = scanner.next()
      if(tmp._1.toUpperCase == "OR"){
        val tmp1 = expANDParse(tmp._2, Nil)
        expParse(tmp1._2, List[Any]("OR", cur, tmp1._1))
      }
      else{
        (cur, scanner)
      }
    }
  }

  def funListParse(scanner: Scanner, ans:List[Any]): (List[Any], Scanner) = {//return a list with out DEFAULT prefix!!!
    val tmp = scanner.next()
    if(tmp._1 == ","){
      funListParse(tmp._2, ans)
    }
    else{
      val tmp1 = tmp._2.next()
      if(tmp1._1 == "("){
        val para = funListParse(tmp1._2, Nil)
        val item = tmp._1 :: para._1
        val tmp2 = para._2.next()
        if(tmp2._2.next()._1==","){ funListParse(tmp2._2, item :: ans) }
        else{ ((item :: ans).reverse, tmp2._2)}
      }
      else if(tmp1._1 == ","){
        val item = "DEFAULT" :: List(tmp._1)
        funListParse(tmp1._2, item :: ans)
      }
      else{
        val item = "DEFAULT" :: List(tmp._1)
        ((item :: ans).reverse, tmp._2)
      }
    }
  }
}

import scala.util.Random

object TestListParser {
  def main(args: Array[String]): Unit = {
    val pr = new Parser
    val ansf = pr.funListParse(new Scanner(" AVE(  a,  'b  ahe' , -0.2e2.0, +9e8  ),  MIN(a), b , c,,  ", 0), Nil)
    //println(ansf)
    val ansexp = pr.expParse(new Scanner("abs(1)>2 ", 0),Nil)
    println(ansexp)
    /*
    import scala.io.Source
    import java.io._
    val sc = Source.fromFile("exp.txt")
    val outf = new PrintWriter(new File("scala_res.txt"))
    for(line <- sc.getLines()) {
      val calstr = line
      val ansexpcal = pr.expCal(new Scanner(calstr, 0), ("", mutable.Map(), mutable.Map()), Array(), true, "AND")
      if(ansexpcal._1){ outf.write("True\n")}
      else{ outf.write("False\n")}
      //println(line)
    }
    outf.close()
    */
  }
}