
import java.io.{BufferedInputStream, BufferedOutputStream, File, PrintStream}
import java.net.ServerSocket
import java.util

import MyScanner.Scanner
import MyParser.Parser
import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.hadoop.fs._

import scala.collection.mutable
import scala.io.BufferedSource
import scala.math._


object Cst{
  val SPACE=Character.toString(1.toChar)
}

object Funs{
  val AggregateFuns = Set("SUM", "AVE", "MAX", "MIN")
  val NormalFuns = Set("ABS","YEAR","MOUTH","SQRT","LOG10")

  def isColName(s:String): Boolean ={
    if(s.length<=0) false
    else{
      if((s(0)>='a' && s(0)<='z') || (s(0)>='A' && s(0)<='Z')) true
      else false
    }
  }

  def isList(v:Any):Boolean = {
    v match{
      case _:List[Any]=>true
      case _=>false
    }
  }

  def aggFunsFlatten(fun: List[Any]):List[Any]={
    val funName = fun.head.toString.toUpperCase()
    val par = fun.tail
    val newPar = par.map(f=>
      if(isList(f)){defaultFlatten(f.asInstanceOf[List[Any]])}
      else{f}
    )
    val nNewPar=newPar.flatMap(f=>
      if(isList(f) && AggregateFuns.contains(f.asInstanceOf[List[Any]].head.toString())){
        f.asInstanceOf[List[Any]].tail
      }
      else{
        List(f)
      }
    )
    funName :: nNewPar
  }

  def defaultFlatten(fun: List[Any]):List[Any]={
    val funName = fun.head.toString.toUpperCase()
    val par = fun.tail
    val newPar = par.map(f=>
      if(isList(f)){defaultFlatten(f.asInstanceOf[List[Any]])}
      else{f}
    )
    val nNewPar=newPar.flatMap(f=>
      if(isList(f) && f.asInstanceOf[List[Any]].head.toString=="DEFAULT"){
        f.asInstanceOf[List[Any]].tail
      }
      else{
        List(f)
      }
    )
    funName :: nNewPar
  }

  def calNormalFun(fun: List[Any]):List[Any]={
    val funName = fun.head.toString.toUpperCase()
    val par = fun.tail
    val newPar = par.map(f=>
      if(isList(f)){calNormalFun(f.asInstanceOf[List[Any]])}
      else{ f }
    )

    def canCal(parL:List[Any]):Boolean={
      if(parL.length<=0) return true
      if(isList(parL.head)){
        val par = parL.head.asInstanceOf[List[Any]]
        if(par.head.toString!="DEFAULT"){ return false }
        else{
          val flag = canCal(par)
          if(!flag) return false
        }
      }
      canCal(parL.tail)
    }

    if(!canCal(newPar)){ return funName :: newPar }
    else{
      if(funName=="ABS"){
        val nNewPar = newPar.map(f=> {
          val para = if (isList(f)){ f.asInstanceOf[List[Any]].tail.head.asInstanceOf[(String, Any)] }
                     else { f.asInstanceOf[(String,Any)] }
          val vtype = para._1.toString
          val vvalue = para._2

          if(vtype=="INT"){ (vtype, abs(vvalue.toString.toInt)) }
          else if(vtype=="FLOAT"){ (vtype, abs(vvalue.toString.toFloat))}
          else{ (vtype, "0")}
        })
        return "DEFAULT" :: nNewPar
      }
      else if(funName==">" || funName=="<" || funName =="=" || funName==">=" || funName=="<=" || funName=="!=" || funName=="=="){
        val par1 = if(isList(newPar.head)){ newPar.head.asInstanceOf[List[Any]].tail.head.asInstanceOf[(String,Any)] }
                   else{newPar.head.asInstanceOf[(String,Any)]}
        val par2 = if(isList(newPar.tail.head)){ newPar.tail.head.asInstanceOf[List[Any]].tail.head.asInstanceOf[(String,Any)] }
                   else{newPar.tail.head.asInstanceOf[(String,Any)]}
        val vtype1 = par1._1; val vtype2 = par2._1
        val vvalue1 = par1._2.toString; val vvalue2 = par2._2.toString

        val ans:Boolean = if(vtype1=="FLOAT" || vtype2=="FLOAT" ){
          if(funName==">"){vvalue1.toFloat > vvalue2.toFloat }
          else if(funName=="<"){vvalue1.toFloat < vvalue2.toFloat }
          else if(funName=="=" || funName=="=="){vvalue1.toFloat == vvalue2.toFloat }
          else if(funName==">="){vvalue1.toFloat >= vvalue2.toFloat }
          else if(funName=="<="){vvalue1.toFloat <= vvalue2.toFloat }
          else if(funName=="!="){vvalue1.toFloat != vvalue2.toFloat }
          else false
        }else if(vtype1=="INT" || vtype2=="INT"){
          if(funName==">"){vvalue1.toInt > vvalue2.toInt }
          else if(funName=="<"){vvalue1.toInt < vvalue2.toInt }
          else if(funName=="=" || funName=="=="){vvalue1.toInt == vvalue2.toInt }
          else if(funName==">="){vvalue1.toInt >= vvalue2.toInt }
          else if(funName=="<="){vvalue1.toInt <= vvalue2.toInt }
          else if(funName=="!="){vvalue1.toInt != vvalue2.toInt }
          else false
        }else{
          if(funName==">"){vvalue1 > vvalue2 }
          else if(funName=="<"){vvalue1 < vvalue2 }
          else if(funName=="=" || funName=="=="){vvalue1 == vvalue2 }
          else if(funName==">="){vvalue1 >= vvalue2 }
          else if(funName=="<="){vvalue1 <= vvalue2 }
          else if(funName=="!="){vvalue1 != vvalue2 }
          else false
        }


        val ansInt=if(ans){1}else{0}
        return List("DEFAULT", ("INT", ansInt))

      }
      else if(funName=="AND" || funName=="OR") {
        val par1 = if (isList(newPar.head)) { newPar.head.asInstanceOf[List[Any]].tail.head.asInstanceOf[(String, Any)]}
                   else { newPar.head.asInstanceOf[(String, Any)]}
        val par2 = if (isList(newPar.tail.head)) { newPar.tail.head.asInstanceOf[List[Any]].tail.head.asInstanceOf[(String, Any)]}
                   else { newPar.tail.head.asInstanceOf[(String, Any)] }
        val vvalue1 = par1._2.toString.toInt
        val vvalue2 = par2._2.toString.toInt

        val ans =  if(funName=="AND"){
          if(vvalue1!=0 && vvalue2!=0) 1
          else 0
        }else{
          if(vvalue1!=0 || vvalue2!=0) 1
          else 0
        }

        return List("DEFAULT", ("INT", ans))

      }
      else if(funName == "YEAR" || funName=="MONTH" || funName=="DAYOFMONTH"){
        val nNewPar = newPar.map(f=> {
          val para = if(isList(f)){ f.asInstanceOf[List[Any]].tail.head.asInstanceOf[(String,String)]}
                     else{ f.asInstanceOf[(String,String)]}
          val vtype = para._1
          val vvalue = para._2

          val dateStr = vvalue.split(" ")(0).split("-")
          if(funName=="YEAR"){("STRING",dateStr(0))}
          else if(funName=="MONTH"){("STRING", dateStr(1))}
          else{("STRING",dateStr(2))}
        })
        return "DEFAULT" :: nNewPar
      }
      else{
        return funName :: newPar
      }
    }
  }

  def reducefun(lA:List[Any], lB:List[Any]):List[Any]={//every para of a aggregate function is like [1,2,3,...]; there is no other functions in the paras;
    val funName = lA.head.toString.toUpperCase()
    if(funName=="SUM"){
      val sumA = lA.tail.reduce((a,b)=>{ val ans=(a.asInstanceOf[(String,String)]._2.toString.toFloat + b.asInstanceOf[(String,String)]._2.toFloat)
        (a.asInstanceOf[(String,String)]._1, ans.toString)
      })
      val sumB = lB.tail.reduce((a,b)=>{ val ans=(a.asInstanceOf[(String,String)]._2.toString.toFloat + b.asInstanceOf[(String,String)]._2.toFloat)
        (a.asInstanceOf[(String,String)]._1, ans.toString)
      })
      val sum = sumA.asInstanceOf[(String,String)]._2.toFloat + sumB.asInstanceOf[(String,String)]._2.toFloat
      return "SUM" :: List(( lA.tail.head.asInstanceOf[(String,String)]._1, sum.toString))
    }
    else{
      val lAB = lA.tail.zip(lB.tail)
      val newlAB = lAB.map(f=>
        if(isList(f._1)){
          val rf = reducefun(f._1.asInstanceOf[List[Any]], f._2.asInstanceOf[List[Any]])
          rf
        }else{
          f._1
        }
      )
      return funName :: newlAB
    }
  }

  def mapfun(s:String, sql: String,
             tableInfo:(String,mutable.Map[String, Int],mutable.Map[String,String])):(String, List[Any])={

    val scanner = new Scanner(sql, 0).next()._2
    val pr = new Parser
    val ans1 = pr.funListParse(scanner, Nil)
    val selList = "DEFAULT" :: ans1._1

    val ans2 = ans1._2.next()._2.next()._2.next()//skip the "from table"

    val conScanner =  if(ans2._1.toUpperCase() == "WHERE"){ ans2._2 }else{ new Scanner("1",0)}
    val conPass = pr.expParse(conScanner, Nil)
    val conList = conPass._1

    val gSC0 = if(ans2._1.toUpperCase()=="WHERE"){ conPass._2 }else{ ans1._2.next()._2.next()._2 }
    val groupScanner=if(gSC0.next()._1.toUpperCase() == "GROUP"){ gSC0.next()._2 }else{ new Scanner("GROUPALL(*)",0)}
    val groupList = "DEFAULT" :: pr.funListParse(groupScanner, Nil)._1

    val item = s.split(Cst.SPACE)

    def repFun(fun: List[Any]):List[Any]={//replace the identifier with (type,value)
      val funName = fun.head.toString.toUpperCase()
      val par = fun.tail
      val newPar = par.map(f=>
        if(isList(f)){ repFun(f.asInstanceOf[List[Any]]) }
        else{
          if(isColName(f.toString)){ (tableInfo._3(f.toString), item(tableInfo._2(f.toString))) }
          else if(f.toString=="*"){
            "DEFAULT" :: (tableInfo._3.keySet.map(g=>(tableInfo._3(g.toString), item(tableInfo._2(g.toString)))).toList)
            //"DEFAULT" :: item.toList.map(g=>(tableInfo._3(g.toString), g.toString))
          }
          else if(f.toString()(0)=='\''){ ("STRING", f.toString.substring(1, f.toString.length-1)) }
          else if(f.toString.contains("e") || f.toString.contains("E") || f.toString.contains(".")){
            ("FLOAT", f.toString)
          }
          else{
            ("INT",f.toString)
          }
        }
      )

      funName :: newPar
    }

    val flag = defaultFlatten(calNormalFun(repFun(conList))).tail.head.asInstanceOf[(String,Any)]._2.toString != "0"

    if(!flag){ ("",Nil) }
    else{
      val gl = defaultFlatten(calNormalFun(repFun(groupList)))//group list
      val glStr = gl.tail.toString()//group list -> str
      val sl = defaultFlatten(calNormalFun(repFun(selList)))//select list
      (glStr, sl)
    }

  }
}

object TSQL {

  val tablePath="hdfs://localhost:9000/Tables"
  val tableList = getTableList()
  val tableInfo = mutable.Map[String,(String, mutable.Map[String,Int], mutable.Map[String,String])]()

  for( tn <- tableList){
    tableInfo += (tn -> getTableInfo(tn))
  }

  def getTableList():Array[String]={
    val conf = new SparkConf().setAppName("GetTableList")
    val sc = new SparkContext(conf)
    val fList = FileSystem.get( sc.hadoopConfiguration ).globStatus( new Path(tablePath + "/" + "*.info"))
    val ans=new Array[String](fList.length)
    for(i <- 0 until fList.length){
      ans(i) = fList(i).getPath.getName
      val ln = ans(i).length
      ans(i) = ans(i).substring(0, ln-5)
    }
    sc.stop()
    ans
  }

  def getTableInfo(tname: String):(String, mutable.Map[String,Int], mutable.Map[String,String])={
    val conf = new SparkConf().setAppName("GetTableInfo " + tname)
    val sc = new SparkContext(conf)
    val rdd = sc.textFile(tablePath + "/" + tname + ".info")
    val lines = rdd.map(l=>l.split(Character.toString(1.toChar))).collect()
    val des = lines(0).mkString(" ")
    val colName = mutable.Map[String, Int]()
    val colType = mutable.Map[String, String]()
    for(i <- 0 until lines(1).length){
      colName += (lines(1)(i)->i)
      colType += (lines(1)(i)->lines(2)(i))
    }
    sc.stop()
    (des, colName, colType)
  }

  def cmd_SHOW(sc: Scanner): Array[List[Any]] = {
    val sc1 = sc.next()
    val pa = sc1._1.toUpperCase()
    if(pa=="TABLES"){
      Array(tableList.toList)
    }else{
      Array(List())
    }
  }
  val conf = new SparkConf().setAppName("select")
  val sc = new SparkContext(conf)

  def cmd_SELECT(sql: String): Array[List[Any]]= {

    try {
      val scanner = new Scanner(sql, 0).next()._2
      val pr = new Parser
      val ans1 = pr.funListParse(scanner, Nil)
      val tname = ans1._2.next()._2.next()._1

      val tInfo = ("hello", tableInfo(tname)._2, tableInfo(tname)._3)
      val rdd = sc.textFile(tablePath + "/" + tname)

      val rdd1 = rdd.map(Funs.mapfun(_, sql, tInfo)).filter(a => a._1.length > 0)
      val rdd2 = rdd1.reduceByKey(Funs.reducefun)
      val rdd3 = rdd2.map(f => (f._1, Funs.defaultFlatten(Funs.calNormalFun(Funs.aggFunsFlatten(f._2)))))

      val ans = rdd3.collect().map(f => f._2.tail.map(g=>g.asInstanceOf[(String,String)]._2.asInstanceOf[Any]))
      return ans

    }catch{
      case _:Any => {
        return Array()
      }
    }finally {
      //sc.stop()
    }
  }

  def query(sql: String): Array[List[Any]] ={
    val scanner = new Scanner(sql, 0)
    val ans = scanner.next()
    val cmd = ans._1.toUpperCase()
    if(cmd == "SELECT"){
      cmd_SELECT(sql)
    }else if(cmd == "SHOW"){
      cmd_SHOW(ans._2)
    }else{
      Array(List())
    }
  }


  def main(args: Array[String]) {
/*
    //test
    def repFun(fun: List[Any]):List[Any]={//replace the identifier with (type,value)
      val funName = fun.head.toString.toUpperCase()
      val par = fun.tail
      val newPar = par.map(f=>
        if(Funs.isList(f)){ repFun(f.asInstanceOf[List[Any]]) }
        else{("INT",f.toString) }
      )
      funName :: newPar
    }

    import scala.io.Source
    import java.io._
    val sc = Source.fromFile("exp.txt")
    val pr = new Parser
    val outf = new PrintWriter(new File("scala_res.txt"))
    var num=0
    for(line <- sc.getLines()) {
      println(num); num += 1
      val calstr = line
      val ansexpcal = Funs.calNormalFun( repFun( pr.expParse(new Scanner(calstr, 0), Nil)._1))
      if(ansexpcal.tail.head.asInstanceOf[(String,Any)]._2.toString =="0"){ outf.println("False")}
      else{outf.println("True")}
    }
    outf.close()
 */


    val server = new ServerSocket(9999)
    while (true) {
      val s = server.accept()
      s.setSoTimeout(60000)
      val in = new BufferedInputStream(s.getInputStream)
      val out = new BufferedOutputStream(s.getOutputStream())
      try {

        val BLOCKSIZE = 100

        def readOneBlock(leftNum:Int, cur:String):String ={
          if(leftNum==0) return cur
          val c = in.read()
          readOneBlock(leftNum-1, cur + c.toChar.toString)
        }

        def readBlocks(cur:String):String = {
          val num = in.read()
          if(num<=0) return cur
          readBlocks(cur + readOneBlock(num, ""))
        }

        val sql = readBlocks("")
        println("Query:" + sql)
        val ans = query(sql).map(f=>f.mkString(Cst.SPACE)).mkString("\n")
        println("Done")

        def writeOneBlock(point:Int, s:String):Unit={
          if(point<s.length){
            out.write(s(point))
            writeOneBlock(point+1,s)
          }
        }

        def writeBlocks(point:Int, s:String): Unit ={
          val ls = s.length
          val wl = min(ls-point, BLOCKSIZE)
          if(wl<=0){ out.write(0); out.flush() }
          else{
            out.write(wl)
            writeOneBlock(0, s.substring(point, point+wl))
            writeBlocks(point+wl, s)
          }
        }
        writeBlocks(0, ans)
        s.close()
        in.close()
        out.close()

      }catch{
        case err: Any =>{
          println("Error" + err.toString)
          out.write(0);
          out.flush();
          if(!s.isClosed()) s.close()
          //in.close(); out.close()
        }
      }
    }
  }

}
