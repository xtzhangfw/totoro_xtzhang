package main

import (
	"database/sql"
	//	"encoding/json"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	//"os"
)

var DBInfo string = "payment:payment@tcp(127.0.0.1:3306)/"

func Query(cmd string) {
	db, err := sql.Open("mysql", DBInfo+"totoro_xtzhang_sparksql")
	defer db.Close()
	err = db.Ping()
	if err != nil {
		fmt.Println("Ping err")
		return
	}
	db.Query(cmd)
}

func CreateDB() {
	db, err := sql.Open("mysql", DBInfo)
	err = db.Ping()
	if err != nil {
		fmt.Println("Ping err")
		return
	}

	cmd := "DROP DATABASE totoro_xtzhang_sparksql"
	db.Query(cmd)
	cmd = "CREATE DATABASE totoro_xtzhang_sparksql"
	db.Query(cmd)
	db.Close()

	cmd = ` CREATE TABLE tableinfo (
					table_id INT NOT NULL AUTO_INCREMENT,
					table_name VARCHAR(1024) NOT NULL,
					file_name VARCHAR(1024) NOT NULL,
					description VARCHAR(1024),
					PRIMARY KEY(table_id),
					UNIQUE(table_id,table_name)
	)`
	Query(cmd)
	cmd = ` CREATE TABLE analyser ( 
					analyser_id INT NOT NULL AUTO_INCREMENT,
					analyser_name VARCHAR(1024) NOT NULL,
					table_name VARCHAR(1024) NOT NULL,
					table_id INT NOT NULL,
					sql_cmd VARCHAR(1024) ,
					plot_cmd VARCHAR(1024) ,
					PRIMARY KEY(analyser_id),
					UNIQUE(analyser_id,analyser_name)
	)`
	Query(cmd)
	cmd = ` CREATE TABLE table_head_info ( 
					head_id INT NOT NULL AUTO_INCREMENT,
					table_name VARCHAR(1024) NOT NULL,
					table_id INT NOT NULL,
					col_index INT ,
					col_name VARCHAR(1024) NOT NULL,
					col_type VARCHAR(1024) NOT NULL,
					PRIMARY KEY(head_id),
					UNIQUE(head_id)
	)`
	Query(cmd)

}

func main() {
	CreateDB()
	cmd := "INSERT INTO tableinfo (table_name, file_name, description) VALUES ('Placement', 'd_placement', 'placement table')"
	Query(cmd)

	cmd = "INSERT INTO table_head_info (table_name, table_id, col_index, col_name, col_type) VALUES ('d_placement', '1', 0, 'network_id', 'INT')"
	Query(cmd)
	cmd = "INSERT INTO table_head_info (table_name, table_id, col_index, col_name, col_type) VALUES ('d_placement', '1', 1, 'start_date', 'STRING')"
	Query(cmd)
	cmd = "INSERT INTO table_head_info (table_name, table_id, col_index, col_name, col_type) VALUES ('d_placement', '1', 2, 'end_date', 'STRING')"
	Query(cmd)
	cmd = "INSERT INTO table_head_info (table_name, table_id, col_index, col_name, col_type) VALUES ('d_placement', '1', 3, 'budget_model', 'STRING')"
	Query(cmd)
	cmd = "INSERT INTO table_head_info (table_name, table_id, col_index, col_name, col_type) VALUES ('d_placement', '1', 4, 'placement_type', 'STRING')"
	Query(cmd)
	cmd = "INSERT INTO table_head_info (table_name, table_id, col_index, col_name, col_type) VALUES ('d_placement', '1', 5, 'price_model', 'STRING')"
	Query(cmd)
	cmd = "INSERT INTO table_head_info (table_name, table_id, col_index, col_name, col_type) VALUES ('d_placement', '1', 6, 'billing_method', 'STRING')"
	Query(cmd)
	cmd = "INSERT INTO table_head_info (table_name, table_id, col_index, col_name, col_type) VALUES ('d_placement', '1', 7, 'controlling_measure', 'STRING')"
	Query(cmd)

	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, sql_cmd, plot_cmd) VALUES ('Network_Proportion','d_placement','1', 'select network_id,count(*) from d_placement group by network_id', 'pie_ploter,0,1,Network_Proportion')"
	Query(cmd)
	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, sql_cmd, plot_cmd) VALUES ('Budget_Model_Proportion','d_placement','1', 'select budget_model,count(*) from d_placement group by budget_model', 'pie_ploter,0,1,Budget_Model_Proportion')"
	Query(cmd)
	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, sql_cmd, plot_cmd) VALUES ('Placement_Type_Proportion','d_placement','1', 'select placement_type,count(*) from d_placement group by placement_type', 'pie_ploter,0,1,Placement_Type_Proportion')"
	Query(cmd)
	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, sql_cmd, plot_cmd) VALUES ('Price_Model_Proportion','d_placement','1', 'select price_model,count(*) from d_placement group by price_model', 'pie_ploter,0,1,Price_Model_Proportion')"
	Query(cmd)
	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, sql_cmd, plot_cmd) VALUES ('Billing_Method_Proportion','d_placement','1', 'select billing_method,count(*) from d_placement group by billing_method', 'pie_ploter,0,1,Billing_Method_Proportion')"
	Query(cmd)
	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, sql_cmd, plot_cmd) VALUES ('Controlling_Measure_Proportion','d_placement','1', 'select controlling_measure,count(*) from d_placement group by controlling_measure', 'pie_ploter,0,1,Controlling_Measure_Proportion')"
	Query(cmd)
	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, sql_cmd, plot_cmd) VALUES ('Controlling_Measure_ByMonth','d_placement','1', 'select controlling_measure,year(start_date), month(start_date), count(*) from d_placement where year(start_date)=2016 group by controlling_measure,year(start_date),month(start_date)', 'line_ploter,2,3,0,Controlling_Measure_ByMonth,Month,Count')"
	Query(cmd)
	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, sql_cmd, plot_cmd) VALUES ('Billing_Method_ByMonth','d_placement','1', 'select billing_method,year(start_date), month(start_date), count(*) from d_placement where year(start_date)=2016 group by billing_method,year(start_date),month(start_date)', 'area_ploter,2,3,0,Billing_Method_ByMonth,Month,Count')"
	Query(cmd)
	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, sql_cmd, plot_cmd) VALUES ('Price_Model_ByMonth','d_placement','1', 'select price_model,year(start_date), month(start_date), count(*) from d_placement where year(start_date)=2016 group by price_model,year(start_date),month(start_date)', 'area_ploter,2,3,0,Price_Model_ByMonth,Month,Count')"
	Query(cmd)
	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, sql_cmd, plot_cmd) VALUES ('Placement_Type_ByMonth','d_placement','1', 'select placement_type,year(start_date), month(start_date), count(*) from d_placement where year(start_date)=2016 group by placement_type,year(start_date),month(start_date)', 'area_ploter,2,3,0,Placement_Type_ByMonth,Month,Count')"
	Query(cmd)
	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, sql_cmd, plot_cmd) VALUES ('Budget_Model_ByMonth','d_placement','1', 'select budget_model, year(start_date), month(start_date), count(*) from d_placement where year(start_date)=2016 group by budget_model,year(start_date),month(start_date)', 'area_ploter,2,3,0, Budget_Model_ByMonth,Month,Count')"
	Query(cmd)

}
