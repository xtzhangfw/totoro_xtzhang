#!/bin/bash

go run index.go &

cd HiveSQLEngine/out/artifacts/HiveSQLEngine_jar

/opt/spark-2.1.0-bin-hadoop2.7/bin/spark-submit --class HiveSQLEngine --master spark://vm:7077 --total-executor-cores 2 ./HiveSQLEngine.jar 2>/dev/null

