package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	//	"io/ioutil"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	//"io"
	"net"
	"net/http"
	"strings"
	//	"os/exec"
	//	"strconv"
	//	"strings"
)

const (
	BLOCKSIZE int    = 100
	SPACE     string = string(rune(1))
	MAXLINES  int    = 10000
)

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

type TemplateData struct {
	TList []string
}

var templatedata TemplateData

func DBQuery(cmd string) []byte {
	db, err := sql.Open("mysql", "payment:payment@tcp(127.0.0.1:3306)/totoro_xtzhang_sparksql")
	defer db.Close()
	err = db.Ping()
	if err != nil {
		fmt.Println("Ping err")
		return nil
	}

	rows, _ := db.Query(cmd)
	defer rows.Close()
	cols, _ := rows.Columns()
	ln := len(cols)

	ansList := make([][]string, 0)

	for rows.Next() {
		tmp := make([]string, ln)
		tmpInterface := make([]interface{}, ln)
		for i := range tmp {
			tmpInterface[i] = &tmp[i]
		}
		rows.Scan(tmpInterface...)
		ansList = append(ansList, tmp)
	}

	data, _ := json.Marshal(ansList)
	return data
}

func SparkQuery(sql string) string {
	conn, err := net.Dial("tcp", ":9998")
	if err != nil {
		fmt.Println("connect failed")
		return ""
	}
	defer conn.Close()
	writeBlocks(conn, sql)
	return readBlocks(conn)

}

func writeBlocks(conn net.Conn, s string) {
	buf := make([]byte, 1)
	p, ls := 0, len(s)
	for p < ls {
		wl := min(ls-p, BLOCKSIZE)
		buf[0] = byte(wl)
		conn.Write(buf)
		conn.Write([]byte(s[p : p+wl]))
		p = p + wl
	}
	buf[0] = 0
	conn.Write(buf)
}

func readBlocks(conn net.Conn) string {
	ans := make([]byte, 0, 4096)
	for true {
		oneBuf := make([]byte, 1)
		_, err := conn.Read(oneBuf)
		num := int(oneBuf[0])
		if err != nil || num == 0 {
			break
		}
		for num > 0 {
			conn.Read(oneBuf)
			ans = append(ans, oneBuf[0])
			num -= 1
		}
	}
	return string(ans)
}

func str2json(s string, maxnum int) []byte {
	strList := strings.Split(s, "\n")
	if maxnum <= 0 {
		maxnum = len(strList)
	}
	maxnum = min(maxnum, len(strList))

	ans := make([][]string, 0, maxnum)
	for i := 0; i < maxnum; i++ {
		ans = append(ans, strings.Split(strList[i], SPACE))
	}
	ansJson, _ := json.Marshal(ans)
	return ansJson
}

func QueryHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()
		qtype := r.Form["qtype"][0]
		fmt.Println(qtype)
		if qtype == "SQL" {
			sql_cmd := r.Form["sql_cmd"][0]
			fmt.Println(sql_cmd)
			ans := SparkQuery(sql_cmd)
			w.Write(str2json(ans, MAXLINES))

		} else if qtype == "PLOT" {
			w.Write([]byte("PLOT"))

		} else if qtype == "COM" {
			cmd := r.Form["com_cmd"][0]
			fmt.Println(r)

			if cmd == "tableinfo" {
				ansJson := DBQuery("select * from tableinfo")
				w.Write(ansJson)

			} else if cmd == "analyser" {
				table_name := r.Form["table_name"][0]
				ansJson := DBQuery("select * from analyser where table_name='" + table_name + "'")
				w.Write(ansJson)

			} else if cmd == "tableheadinfo" {
				table_name := r.Form["table_name"][0]
				ansJson := DBQuery("select * from table_head_info where table_name='" + table_name + "'" + "ORDER BY col_index")
				w.Write(ansJson)
			} else {
			}

		} else {
			w.Write([]byte(qtype))
		}
	}
}

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	t, err := template.ParseFiles("templates/index.html")
	if err != nil {
		fmt.Println("Parse err")
		return
	}
	t.Execute(w, templatedata)
}

func main() {
	templatedata.TList = make([]string, 10)
	for i := 0; i < 10; i++ {
		str := fmt.Sprintf("Table %d", i)
		fmt.Println(str)
		templatedata.TList[i] = str
	}
	http.HandleFunc("/", IndexHandler)
	http.HandleFunc("/Query", QueryHandler)
	fs := http.FileServer(http.Dir("/home/zxt/IdeaProjects/totoro_xtzhang/test5/static/"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))
	http.ListenAndServe(":8887", nil)
}
