
import java.io.{BufferedInputStream, BufferedOutputStream, File, PrintStream}
import java.net.ServerSocket
import java.util

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.hadoop.fs._
import org.apache.spark.sql.SQLContext

import scala.collection.mutable
import scala.io.BufferedSource
import scala.math._


object Cst{
  val SPACE=Character.toString(1.toChar)
}


object SparkSQLEngine {

  val tablePath="hdfs://localhost:9000/Tables"
  val tableInfo = mutable.Map[String,(String, mutable.Map[String,Int], mutable.Map[String,String])]()


  val conf = new SparkConf().setAppName("select from parquet")
  val sc = new SparkContext(conf)
  val sqlContext = new SQLContext(sc)
  val parqFile = sqlContext.read.parquet("/d_placement.parquet")
  parqFile.registerTempTable("d_placement")

  def json2parquet(): Unit={
    val conf = new SparkConf().setAppName("Json2Parquet")
    val sc = new SparkContext(conf)
    val sqlContext = new SQLContext(sc)
    val data = sqlContext.read.json("/d_placement.json")
    data.write.parquet("/d_placement.parquet")
    sc.stop()
  }

  def query(sql: String): Array[org.apache.spark.sql.Row] ={
    val ans = sqlContext.sql(sql).collect()
    return ans
  }


  def main(args: Array[String]) {
    //json2parquet()
    val server = new ServerSocket(9998)
    while (true) {
      val s = server.accept()
      s.setSoTimeout(60000)
      val in = new BufferedInputStream(s.getInputStream)
      val out = new BufferedOutputStream(s.getOutputStream())
      try {

        val BLOCKSIZE = 100

        def readOneBlock(leftNum:Int, cur:String):String ={
          if(leftNum==0) return cur
          val c = in.read()
          readOneBlock(leftNum-1, cur + c.toChar.toString)
        }

        def readBlocks(cur:String):String = {
          val num = in.read()
          if(num<=0) return cur
          readBlocks(cur + readOneBlock(num, ""))
        }

        val sql = readBlocks("")
        println("Query:" + sql)
        val ans = query(sql).map(f=>f.mkString(Cst.SPACE)).mkString("\n")
        println("Done")

        def writeOneBlock(point:Int, s:String):Unit={
          if(point<s.length){
            out.write(s(point))
            writeOneBlock(point+1,s)
          }
        }

        def writeBlocks(point:Int, s:String): Unit ={
          val ls = s.length
          val wl = min(ls-point, BLOCKSIZE)
          if(wl<=0){ out.write(0); out.flush() }
          else{
            out.write(wl)
            writeOneBlock(0, s.substring(point, point+wl))
            writeBlocks(point+wl, s)
          }
        }
        writeBlocks(0, ans)
        s.close()
        in.close()
        out.close()

      }catch{
        case err: Any =>{
          println("Error" + err.toString)
          out.write(0);
          out.flush();
          if(!s.isClosed()) s.close()
          //in.close(); out.close()

        }
      }
    }
  }

}
