package main

import (
	"encoding/json"
	"fmt"
	"os"
	"strconv"
	"bufio"
	"strings"
	"io"
)


type DBLine struct{
  Network_id int `json:"network_id"`
  Start_date  string `json:"start_date"`
  End_date string `json:"end_date"`
  Budget_model string `json:"budget_model"`
  Placement_type string `json:"placement_type"`
  Price_model string `json:"price_model"`
  Billing_method string `json:"billing_method"`
  Controlling_measure string `json:"controlling_measure"`
}

func TxtToJson() {

        db_list := make([]DBLine, 0)

	fin, _ := os.Open("d_placement.txt")
	fout, _ := os.Create("d_placement.json")
	buf := bufio.NewReader(fin)
        for {
          line, err := buf.ReadString('\n')
          if err==io.EOF { break }
          tmp := strings.Split(line, string(rune(1)))
	  var ditem DBLine
	  ditem.Network_id,_ = strconv.Atoi(tmp[0])
	  ditem.Start_date = tmp[1]
	  ditem.End_date = tmp[2]
	  ditem.Budget_model = tmp[3]
	  ditem.Placement_type = tmp[4]
	  ditem.Price_model = tmp[5]
	  ditem.Billing_method = tmp[6]
	  ditem.Controlling_measure = tmp[7]
	  db_list = append(db_list, ditem)
	}
	data, err := json.Marshal(db_list)

	if err!=nil {
	  fmt.Println("err")
	}
        fout.Write(data)
}

func main() {
  TxtToJson()
}
