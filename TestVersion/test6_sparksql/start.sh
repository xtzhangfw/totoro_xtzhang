#!/bin/bash

go run index.go &

cd SparkSQLEngine/out/artifacts/SparkSQLEngine_jar

/opt/spark-2.1.0-bin-hadoop2.7/bin/spark-submit --class SparkSQLEngine --master spark://vm:7077 --total-executor-cores 2 ./SparkSQLEngine.jar 2>/dev/null

