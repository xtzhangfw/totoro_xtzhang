#!/bin/bash

/opt/hadoop-2.8.0/sbin/stop-all.sh
/opt/spark-2.1.0-bin-hadoop2.7/sbin/stop-all.sh

/opt/hadoop-2.8.0/sbin/start-all.sh
/opt/spark-2.1.0-bin-hadoop2.7/sbin/start-all.sh
