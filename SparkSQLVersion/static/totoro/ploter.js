function plot_line_gra(plot_id){
	$("#plot_canvas_" + plot_id).highcharts({})
	var data_map_list=new Array();

  var jsonObj = plot_list[plot_id].plot_json;
  var row = jsonObj.length;
	if(row<=0) return;
  var col =jsonObj[0].length;

	var paras = plot_list[plot_id].plot_para;
	
  var x_col = parseInt(paras[1]);
  var y_col = parseInt(paras[2]);
  var g_col = parseInt(paras[3]);
	var title = paras[4];
	var x_title = paras[5];
	var y_title = paras[6];

  var data_map=new Map();

  for(var i=0; i<row; i++){
    var g_name = "";
    if(g_col>=0 && g_col<col){
      g_name = jsonObj[i][g_col];
    }
    var x = parseFloat(jsonObj[i][x_col]); 
    var y = parseFloat(jsonObj[i][y_col]);

    if(!data_map.has(g_name)){
      data_map.set(g_name, new Array());
    }
    data_map.get(g_name).push(new Array(x,y));
  }  

  data_map.forEach(function(item,key,obj){
    item.sort(function(a,b){return a[0]-b[0];})
    data_map_list.push({name:key, data:item});
  });
	
	
	$("#plot_canvas_" + plot_id).highcharts({
    chart: {
      type: 'spline'
    },
    title: {
      text: title
    },
    subtitle: {
      text: ''
    },
    xAxis: {
      title: {
        text: x_title
      }
    },
    yAxis: {
      title: {
        text: y_title
      },
      min: 0
    },
    tooltip: {
      headerFormat: '<b>{series.name}</b><br>',
      pointFormat: '{point.x}, {point.y}'
    },
    plotOptions: {
      spline: {
        marker: {
          enabled: true
        }
      }
    },
    series: data_map_list
  });
}


function plot_area_gra(plot_id){
	$("#plot_canvas_" + plot_id).highcharts({})
	
	var data_map_list=new Array();

  var jsonObj = plot_list[plot_id].plot_json;
  var row = jsonObj.length;
	if(row<=0) return;
  var col =jsonObj[0].length;

	var paras = plot_list[plot_id].plot_para;
	
  var x_col = parseInt(paras[1]);
  var y_col = parseInt(paras[2]);
  var g_col = parseInt(paras[3]);
	var title = paras[4];
	var x_title = paras[5];
	var y_title = paras[6];

  var data_map=new Map();

  for(var i=0; i<row; i++){
    var g_name = "";
    if(g_col>=0 && g_col<col){
      g_name = jsonObj[i][g_col];
    }
    var x = parseFloat(jsonObj[i][x_col]); 
    var y = parseFloat(jsonObj[i][y_col]);

    if(!data_map.has(g_name)){
      data_map.set(g_name, new Array());
    }
    data_map.get(g_name).push(new Array(x,y));
  }  

  data_map.forEach(function(item,key,obj){
    item.sort(function(a,b){return a[0]-b[0];})
    data_map_list.push({name:key, data:item});
  });
	
	
	$("#plot_canvas_" + plot_id).highcharts({
    chart: {
      type: 'area'
    },
    title: {
      text: title
    },
    subtitle: {
      text: ''
    },
    xAxis: {
      title: {
        text: x_title
      }
    },
    yAxis: {
      title: {
        text: y_title
      },
      min: 0
    },
    tooltip: {
      headerFormat: '<b>{series.name}</b><br>',
      pointFormat: '{point.x}, {point.y}'
    },
    plotOptions: {
      spline: {
        marker: {
          enabled: true
        }
      }
    },
    series: data_map_list
  });
}


function plot_pie_gra(plot_id){
  var data_list=new Array();
	$("#plot_canvas_" + plot_id).highcharts({})
	
	var jsonObj = plot_list[plot_id].plot_json;
  var row = jsonObj.length;
	if(row<=0) return;
  var col =jsonObj[0].length;

	var paras = plot_list[plot_id].plot_para;
	var x_col = parseInt(paras[1]);
	var y_col = parseInt(paras[2]);
	var group_col = parseInt(paras[3]);
	var title = paras[4];
	var x_title = paras[5];
	var y_title = paras[6];
  
  for(var i=0; i<row; i++){
    var x = (jsonObj[i][x_col]); 
    var y = parseFloat(jsonObj[i][y_col]);
    data_list.push(new Array(x,y));
  }  

  $("#plot_canvas_" + plot_id).highcharts({
    chart: {
      plotBackgroundColor: null,
      plotBorderWidth: null,
      plotShadow: false
    },
    title: {
      text: title
    },
    tooltip: {
      headerFormat: '{series.name}<br>',
      pointFormat: '{point.name}: <b>{point.percentage:.1f}%</b>'
    },
    plotOptions: {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        dataLabels: {
          enabled: true,
          format: '<b>{point.name}</b>: {point.percentage:.1f} %',
          style: {
            color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
          }
        }
      }
    },
    series: [{
      type: 'pie',
      name: title,
      data: data_list
    }]
  });

}
