function Plot_Obj(plot_id, plot_json, plot_sql, plot_para){
	this.plot_json = plot_json;
	this.plot_sql = plot_sql;
	this.plot_para = plot_para.split(String.fromCharCode(1));
	this.plot_id = plot_id;
	this.h_size  = 5;
	var obj = this;
	
	this.get_data = function(){
		AJAX("cmd=sparksql&sql_cmd=" + obj.plot_sql, function(res){
			var data = JSON.parse(res);
			obj.plot_json = data.slice(1);
			obj.col_names = data[0];
		})
	};
	this.get_data();
	this.sta_select = 0;

	this.equal = function(obj2){
		if(obj.plot_sql != obj2.plot_sql) return false;
		
		if(obj.plot_para.length != obj2.plot_para.length) return false;
		else{
			for(var i=0; i<obj.plot_para.length; i++){
				if(obj.plot_para[i] != obj2.plot_para[i]) return false;
			}
		}

		if(obj.col_names.length != obj2.col_names.length) return false;
		else{
			for(var i=0; i<obj2.col_names.length; i++){
				if(obj.col_names[i] != obj2.col_names[i]) return false;
			}
		}

		var row = obj.plot_json.length;
		var row2 = obj2.plot_json.length;
		if(row != row2) return false;
		else if(row>0){
			var col = obj.plot_json[0].length;
			var col2 = obj2.plot_json[0].length;
			if(col != col2) return false;
			for(var i=0; i<row; i++){
				for(var j=0; j<col; j++){
					if(obj.plot_json[i][j]!= obj2.plot_json[i][j]) return false;
				}
			}
		}
		return true;
	}

	
	var topid = "#plot_h_top_" + plot_id;
	var leftid = "#plot_h_left_" + plot_id;
	var rightid = "#plot_h_right_" + plot_id;
	var bottomid = "#plot_h_bottom_" + plot_id;
	var canvasid = "#plot_canvas_" + plot_id;
	var h_size = this.h_size;

	this.move = function(x,y){
		$(canvasid).css({left:x + "px", top: y + "px"});
		obj.arrange();
	}

	this.move_to_last = function(){
		var plot_grid = row_arrange_array();
		var row_num = plot_grid.length;
		if(row_num>0){
			var col_num = plot_grid[row_num-1].length;
			var last_id = plot_grid[row_num-1][col_num-1];
			var last_obj = plot_list[last_id];
			var dh = 0;
			if(plot_grid[row_num-1].indexOf(obj.plot_id)>=0){
				col_num -= 1;
			}
			if(col_num >= 4){ dh += 9999; }
			obj.move(last_obj.left()+1, last_obj.top()+1+dh);
		}
	}

	this.move_random = function(){
		var s_left = $("#contentbar_div").scrollLeft();
		var s_top = $("#contentbar_div").scrollTop();
		
		var rand1 = parseInt(Math.random()*10000000);
		var rand2 = parseInt(Math.random()*10000000);
		
		var content_width = $("#contentbar_div").width();
		var content_height = $("#contentbar_div").height();
		var width = $(canvasid).outerWidth();
		var height = $(canvasid).outerHeight();

		var top_rand = s_top + rand1%(content_height - height);
		var left_rand = s_left + rand2%(content_width - width);

		obj.move(left_rand, top_rand);
	}

	this.resize = function(w,h){
		$(canvasid).css({width:w + "px", height: h + "px"});
		obj.plot_gra();
		obj.arrange();
	}

	this.left = function(){
		var pos = $(canvasid).position();
		pos.left += $("#contentbar_div").scrollLeft();
		pos.top += $("#contentbar_div").scrollTop();
		return pos.left;
	}

	this.top = function(){
		var pos = $(canvasid).position();
		pos.left += $("#contentbar_div").scrollLeft();
		pos.top += $("#contentbar_div").scrollTop();
		return pos.top;
	}
	
	this.arrange = function(){
		if(plot_list[plot_id].sta_select!=0){
			$(canvasid).css({border: "2px solid #A22"});
		}
		else{
			$(canvasid).css({border: "1px solid #AAA"});
		}

		var cw = $(canvasid).outerWidth(); var ch = $(canvasid).outerHeight();
		if(cw<=100){
			$(canvasid).outerWidth(100);
			cw=100;
		}
		if(ch<=100){
			$(canvasid).outerHeight(100);
			ch = 100;
		}
		var pos = $(canvasid).position();
		pos.left += $("#contentbar_div").scrollLeft();
		pos.top += $("#contentbar_div").scrollTop();
		var cleft = pos.left; var ctop = pos.top;
		var pad = 5;

		$(topid).css({left:cleft-h_size+"px", top:ctop-h_size+"px", width:cw+h_size*2+"px", height:h_size + "px"});
		$(bottomid).css({left:cleft-h_size+"px", top:ctop+ch+"px", width:cw+h_size*2+"px", height:h_size + "px"});
		$(leftid).css({left:cleft-h_size+"px", top:ctop+"px", width:h_size+"px", height:ch + "px"});
		$(rightid).css({left:cleft+cw+"px", top:ctop+"px", width:h_size+"px", height:ch + "px"});

	}

	this.plot_gra = function(){
		var paras = obj.plot_para;
		if(paras[0]=="line_ploter"){
			plot_line_gra(plot_id);
		}
		else if(paras[0]=="pie_ploter"){
			plot_pie_gra(plot_id);
		}
		else if(paras[0]=="area_ploter"){
			plot_area_gra(plot_id);
		}
	}

	this.hide_plot = function(){
		$(topid).hide();
		$(bottomid).hide();
		$(leftid).hide();
		$(rightid).hide();
		$(canvasid).hide();
	}

	this.plot = function(){
		var top_str = "<div class='plot_handler plot_handler_top' id='plot_h_top_" + plot_id + "'></div>";
		var left_str = "<div class='plot_handler plot_handler_left' id='plot_h_left_" + plot_id + "'></div>";
		var right_str = "<div class='plot_handler plot_handler_right' id='plot_h_right_" + plot_id + "'></div>";
		var bottom_str = "<div class='plot_handler plot_handler_bottom' id='plot_h_bottom_" + plot_id + "'></div>";
		var canvas_str = "<div class='plot_canvas_div' id='plot_canvas_" + plot_id + "'>canvas</div>";
		
		$("#contentbar_div").append(top_str + left_str + right_str + bottom_str + canvas_str);

		$(canvasid).draggable({
			start:function(){
				cancel_all_select();
				obj.sta_select=1;
				obj.arrange();				
			},
			drag:function(){
				obj.arrange();
			},
			stop:function(){
				obj.arrange();
			}
		});

		$(rightid).draggable({
			drag:function(){
				var s_left = $("#contentbar_div").scrollLeft();		var s_top =  $("#contentbar_div").scrollTop();
				var pos=$(rightid).position();
				pos.left += s_left; pos.top += s_top;
				
				$(canvasid).outerWidth(pos.left - ($(canvasid).position().left + s_left));
				obj.arrange();
				obj.plot_gra();
			},
			stop:function(){
				var s_left = $("#contentbar_div").scrollLeft();		var s_top =  $("#contentbar_div").scrollTop();
				var pos=$(rightid).position();
				pos.left += s_left; pos.top += s_top;
				$(canvasid).outerWidth(pos.left - ($(canvasid).position().left + s_left));
				obj.arrange();
				obj.plot_gra();
			},
			axis: 'x'
		});
		
		$(leftid).draggable({
			drag:function(){
				var s_left = $("#contentbar_div").scrollLeft();		var s_top =  $("#contentbar_div").scrollTop();
				var pos=$(leftid).position();
				pos.left += s_left; pos.top += s_top;
				$(canvasid).outerWidth(($(rightid).position().left+s_left) - pos.left- h_size);
				$(canvasid).css({left: pos.left+h_size+"px"});
				obj.arrange();
				obj.plot_gra();
			},
			stop:function(){
				var pos=$(leftid).position();
				var s_left = $("#contentbar_div").scrollLeft();		var s_top =  $("#contentbar_div").scrollTop();
				pos.left += s_left; pos.top += s_top;
				$(canvasid).outerWidth(($(rightid).position().left+s_left) - pos.left- h_size);
				$(canvasid).css({left: pos.left + h_size + "px"});
				obj.arrange();
				obj.plot_gra();
			},
			axis: 'x'
		});

		$(bottomid).draggable({
			drag:function(){
				var s_left = $("#contentbar_div").scrollLeft();		var s_top =  $("#contentbar_div").scrollTop();
				var pos=$(bottomid).position();
				pos.left += s_left; pos.top += s_top;
				$(canvasid).outerHeight(pos.top - ($(canvasid).position().top+s_top));
				obj.arrange();
				obj.plot_gra();
			},
			stop:function(){
				var s_left = $("#contentbar_div").scrollLeft();		var s_top =  $("#contentbar_div").scrollTop();
				var pos=$(bottomid).position();
				pos.left += s_left; pos.top += s_top;
				$(canvasid).outerHeight(pos.top - ($(canvasid).position().top+s_top));
				obj.arrange();
				obj.plot_gra();
			},
			axis: 'y'
		});

		$(topid).draggable({
			drag:function(){
				var s_left = $("#contentbar_div").scrollLeft();		var s_top =  $("#contentbar_div").scrollTop();
				var pos=$(topid).position();
				pos.left += s_left; pos.top += s_top;
				$(canvasid).outerHeight(($(bottomid).position().top+s_top) - pos.top - h_size);
				$(canvasid).css({top:pos.top + h_size + "px"});
				obj.arrange();
				obj.plot_gra();
			},
			stop:function(){
				var s_left = $("#contentbar_div").scrollLeft();		var s_top =  $("#contentbar_div").scrollTop();
				var pos=$(topid).position();
				pos.left += s_left; pos.top += s_top;
				$(canvasid).outerHeight(($(bottomid).position().top+s_top) - pos.top - h_size);
				$(canvasid).css({top:pos.top + h_size + "px"});
				obj.arrange();
				obj.plot_gra();
			},
			axis: 'y'
		});

		$(canvasid).dblclick(function(){
			focus="plot_dialog";
			$("#plot_dialog").modal('show');
		});
		
		$(canvasid).click(function(){
			cancel_all_select();
			obj.sta_select=1;
			obj.arrange();
			focus="plot_obj";
		});
		
		obj.plot_gra();
		obj.arrange();
	}
}

function cancel_select(plot_id){
	plot_list[plot_id].sta_select=0;
	plot_list[plot_id].arrange();
}

function cancel_all_select(){
	for(var i in plot_list){
		cancel_select(i);
	}
}

function max_index(){
	var ans = -99999;
	for(var i in plot_list){
		var zI = parseInt($("#plot_canvas_" + i).css("z-index"));
		ans=Math.max(ans, zI);
	}
	return ans;
}

function min_index(){
	var ans = 99999;
	for(var i in plot_list){
		var zI = parseInt($("#plot_canvas_" + i).css("z-index"));
		ans=Math.min(ans, zI);
	}
	return ans;
}

function get_select_id(){
	for(var i in plot_list){
		if(plot_list[i].sta_select!=0) return i;
	}
	return 0;
}

function move_front(plot_id){
	var zI = max_index() + 1;
	$("#plot_canvas_" + plot_id).css({"z-index": zI});
	$("#plot_h_top_" + plot_id).css({"z-index": zI});
	$("#plot_h_left_" + plot_id).css({"z-index": zI});
	$("#plot_h_bottom_" + plot_id).css({"z-index": zI});
	$("#plot_h_right_" + plot_id).css({"z-index": zI});
}

function move_back(plot_id){
	var zI = min_index() - 1;
	$("#plot_canvas_" + plot_id).css({"z-index": zI});
	$("#plot_h_top_" + plot_id).css({"z-index": zI});
	$("#plot_h_left_" + plot_id).css({"z-index": zI});
	$("#plot_h_bottom_" + plot_id).css({"z-index": zI});
	$("#plot_h_right_" + plot_id).css({"z-index": zI});
}


function delete_plot(plot_id){
	plot_list[plot_id].hide_plot();
	delete plot_list[plot_id];
}

function table_arrange(){
	var plot_array = new Array();
	for(var i in plot_list){
		plot_array.push(i);
	}
	plot_array.sort(function(id1,id2){
		var pos1 = $("#plot_canvas_" + id1).position();
		var pos2 = $("#plot_canvas_" + id2).position();
		return pos1.top - pos2.top;
	});

	var i = 0;
	var pos0 = $("#plot_canvas_" + plot_array[i]).position();
	for(i=1; i<plot_array.length; i++){
		var posi = $("#plot_canvas_" + plot_array[i]).position();
		var hi =  $("#plot_canvas_" + plot_array[i]).outerHeight();

		if(posi.top >= pos0.top + hi/2) break;
	}
	var col_num = i;
	
	var padding = 10;
	var width_total = $("#contentbar_div").width();
	var height_total = $("#contentbar_div").height();
	var width = width_total/col_num;

	var canvas_width = width - padding*2;
	var canvas_height = canvas_width*4.0/5.0;
	if(canvas_height>height_total/2){
		canvas_height = height_total/2;
	}
	if(canvas_height < 200) canvas_height=200;
	
	var height = canvas_height + padding*2;

	for(var i = 0; i<=plot_array.length/col_num; i++){
		for(var j = 0; j<col_num; j++){
			var top = i*height + padding/2;
			var left = j*width + padding/2;
			var index = i*col_num + j;
			var id = plot_array[index];
			var canvasid = "#plot_canvas_" + id;
			$(canvasid).outerWidth(canvas_width);
			$(canvasid).outerHeight(canvas_height);
			$(canvasid).css({left: left + "px", top: top + "px"});
			plot_list[id].arrange();
			plot_list[id].plot_gra();
		}
	}
}

function row_arrange(){
	var plot_grid = row_arrange_array();
	var padding = 10;
	var width_total = $("#contentbar_div").width()-padding;
	var height_total = $("#contentbar_div").height()-padding;

	var top = padding;

	for(var i=0; i<plot_grid.length; i++){
		var col_num = plot_grid[i].length;
		var width = width_total/col_num;
		var canvas_width = width - padding*2;
		var canvas_height = canvas_width*4.0/5.0;
		if(canvas_height > height_total/2){
			canvas_height = height_total/2;
		}
		
		var left = padding;
		
		for(var j=0; j<plot_grid[i].length; j++){
			var id = plot_grid[i][j];
			var canvasid = "#plot_canvas_" + id;
			$(canvasid).outerWidth(canvas_width);
			$(canvasid).outerHeight(canvas_height);
			$(canvasid).css({left: left + "px", top: top + "px"});
			plot_list[id].arrange();
			plot_list[id].plot_gra();
			left += width;
		}
		top += canvas_height + padding;
	}
}

function row_arrange_array(){
	var plot_array = new Array();
	for(var i in plot_list){
		plot_array.push(i);
	}
	plot_array.sort(function(id1,id2){
		var pos1 = $("#plot_canvas_" + id1).position();
		var pos2 = $("#plot_canvas_" + id2).position();
		return pos1.top - pos2.top;
	});

	var plot_grid = new Array();
	var i=0;
	while(i<plot_array.length){
		var plot_row = new Array();

		var j = i + 1;
		var id_i = plot_array[i];
		var pos_i = $("#plot_canvas_" + id_i).position();
		var hi = $("#plot_canvas_" + id_i).outerHeight();

		plot_row.push(id_i);
		
		while(j<plot_array.length){
			var id_j = plot_array[j];
			var pos_j = $("#plot_canvas_" + id_j).position();
			if(pos_j.top >= hi/2 + pos_i.top){break;}
			plot_row.push(id_j);
			j = j + 1;
		}

		plot_row.sort(function(id1, id2){
			var pos1 = $("#plot_canvas_" + id1).position();
			var pos2 = $("#plot_canvas_" + id2).position();
			return pos1.left - pos2.left;
		});

		plot_grid.push(plot_row);
		i = j;
	}
	return plot_grid;
}

function del_repeat_charts(){
	var plot_array = new Array();
	for(var i in plot_list){
		plot_array.push(i);
	}

	for(var i=0; i<plot_array.length; i++){
		for(var j=i+1; j<plot_array.length; j++){
			var id_i = plot_array[i];
			var id_j = plot_array[j];
			if(id_i in plot_list && id_j in plot_list && plot_list[id_i].equal(plot_list[id_j])){
				delete_plot(id_j);
			}
		}
	}
}

function del_all_charts(){
	for(var i in plot_list){
		delete_plot(i);
	}
}


