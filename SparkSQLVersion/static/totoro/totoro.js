//global variables
/////////////////////////////////
var sidebar_width=300;
var tableinfo={};
var analyserinfo={};//[analyser_id, analyser_name, table_name, table_id, user_id, sql_cmd, plot_cmd, description]
var plot_list={};
var cur_table_name;
var focus="";
/////////////////////////////////

$(function(){
	//UI arrange
	arrange();
	
	$("#sidebarhandler_div").draggable({
		drag: function(){
			var pos = $("#sidebarhandler_div").position();
			sidebar_width = pos.left;
			arrange();
		},
		stop: function(){
			var pos = $("#sidebarhandler_div").position();
			sidebar_width = pos.left;
			arrange();
		},
		axis: "x",
		
	});

	$(window).resize(function(){
		arrange();
	});
	////////////////////////////////////////

	AJAX("cmd=tablelist", freshTableList);

	$("[data-toggle='tooltip']").tooltip();

	/////keyboard event ////////////////////////////
	$(document).keydown(function(){
		if((event.keyCode==46 || event.keyCode==8) && focus=="plot_obj"){
			delete_plot(get_select_id());
		}
	});

});


function arrange(){
	var win_height = $(window).height();
	var win_width = $(window).width();
	var headbar_height = 0;
	var footbar_height = 30;
	var toolbar_height = 30;
	var sidebar_height = win_height - headbar_height - footbar_height;
	var contentbar_height = sidebar_height;
	var sidebarhandler_width = 3;
	var contentbar_width = win_width - sidebar_width - sidebarhandler_width;
	
	$("#headbar_div").css({height:headbar_height, position:"absolute", left:0, top:0, width:"100%", 'overflow':"hidden"});
	$("#footbar_div").css({height:footbar_height, position:"absolute", left:0, bottom:0, width:"100%", overflow:"hidden"});
	$("#sidebar_div").css({height:sidebar_height, position:"absolute", left:0, top:headbar_height, width:sidebar_width, overflow:"hidden" });
	$("#sidebarhandler_div").css({height:sidebar_height, position:"absolute", left:sidebar_width, top:headbar_height, width:sidebarhandler_width, overflow:"hidden", cursor:"ew-resize"});
	$("#contentbar_div").css({height:sidebar_height-toolbar_height, position:"absolute", right:0, top:headbar_height + toolbar_height, width: contentbar_width, overflow:"auto"});
	$("#toolbar_div").css({height:toolbar_height, position:"absolute", right:0, top:headbar_height, width: contentbar_width, overflow:"hidden"});
	
}

function freshTableList(res){
  var jsonObj = JSON.parse(res);
  var row = jsonObj.length;
  var col =jsonObj[0].length;
  var inHtml = "";
  for(var i=0; i<row; i++){
    inHtml += ("<a  class='list-group-item tableitem' value='" + jsonObj[i][1] + "'>" + jsonObj[i][1] + "</a>");
		tableinfo[jsonObj[i][1]]=jsonObj[i];
  }
  $("#tablelist_div2").html(inHtml);
	
	$(".tableitem").click(function(){
		var tname = $(this).attr("value");
		cur_table_name = tname;
		AJAX("cmd=analyserlist&table_name=" + tname, freshAnalyserList);
		$("#footbar_div").html(tableinfo[tname][3].replace(new RegExp(String.fromCharCode(1), "g"), "  /  "));
	});
}

function freshAnalyserList(res){
  var jsonObj = JSON.parse(res);
  var row = jsonObj.length;
  var col =jsonObj[0].length;
  var inHtml = "";
  for(var i=0; i<row; i++){
    inHtml += ("<span class='list-group-item' ><input class='analyser_chkbox' type='checkbox'  value='" + jsonObj[i][1] + "'> " + jsonObj[i][1] + "</span>");
		analyserinfo[jsonObj[i][1]]=jsonObj[i];
  }
  $("#analyser_div2").html(inHtml);
}

function AJAX(cmd, funHandler){
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function(){
    if(this.readyState==4 && this.status==200){
			var res = this.responseText;
      funHandler(res)
    }
  };
  xhttp.open("POST","Query",false);
	xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
  xhttp.send(cmd);
}


function analyser_plot(){
	var num = 0;
	$(".analyser_chkbox").each(function(){
		if($(this).is(':checked')){
			num++;
			var analyser_name = $(this).val();
			var analyser_paras = analyserinfo[analyser_name];
			var plot_id = "plot_gra_" + parseInt((Math.random()*10000000));
			var plot_grid = row_arrange_array();

			plot_list[plot_id]=new Plot_Obj(plot_id, "", analyser_paras[5], analyser_paras[6]);
			plot_list[plot_id].plot();
			plot_list[plot_id].move_to_last();

			row_arrange();
			cancel_all_select();
			plot_list[plot_id].sta_select=1;
			plot_list[plot_id].arrange();
		}
		$(this).removeAttr("checked");
	});

	if(num==0){
		var plot_id = add_empty_plot();
		cancel_all_select();
		plot_list[plot_id].sta_select = 1;
		plot_list[plot_id].arrange();
		$("#plot_dialog").modal('show');
	}
}

function add_empty_plot(){
	var plot_id = "plot_gra_" + parseInt((Math.random()*10000000));
	var tmp = new Array("line_ploter","0", "0", "0", "Title", "XTitle", "YTitle","Description");
	plot_list[plot_id]=new Plot_Obj(plot_id, "", "", tmp.join(String.fromCharCode(1)));
	plot_list[plot_id].plot();
	plot_list[plot_id].move_to_last();
	row_arrange();
	return plot_id;
}


function logout(){
	var username = $("#username_div").attr("value");
	AJAX("cmd=logout&username=" + username);
	$(window).attr("location",'/login');
}


function del_sel_analysers(){
	$(".analyser_chkbox").each(function(){
		if($(this).is(':checked')){
			var analyser_name = $(this).val();
			AJAX("cmd=del_analyser&analyser_name=" + analyser_name);
		}
		$(this).removeAttr("checked");
	});
	AJAX("cmd=analyserlist&table_name=" + cur_table_name, freshAnalyserList);
	
}

function on_del_button(){
	if(confirm("Delete these Analyzers?")){
		del_sel_analysers();
	}
}

