var line_para_html=`
	<div class="col-md-6">
		<div class="input-group">
			<span class="input-group-addon">Title</span>
				<input type="text" class="form-control" id="title_input">
		</div>
				<div class="input-group">
					<span class="input-group-addon">X Title</span>
						<input type="text" class="form-control" id="xtitle_input">
				</div>

						<div class="input-group">
							<span class="input-group-addon">Y Title</span>
								<input type="text" class="form-control" id="ytitle_input">
						</div>
								
	</div>

								<div class="col-md-6">
									<div class="input-group">
										<span class="input-group-addon">X-Col</span>
											<select id="xcol_select" class="form-control"></select>
									</div>

												<div class="input-group">
													<span class="input-group-addon">Y-Col</span>
														<select id="ycol_select" class="form-control"></select>
												</div>

															<div class="input-group">
																<span class="input-group-addon">Group</span>
																	<select id="group_select" class="form-control"></select>
															</div>
																		
								</div>
`;


var pie_para_html=`
	<div class="col-md-4">
		<div class="input-group">
			<span class="input-group-addon">Title</span>
				<input type="text" class="form-control" id="title_input">
		</div>
	</div>

				<div class="col-md-4">
					<div class="input-group">
						<span class="input-group-addon">X-Col</span>
							<select id="xcol_select" class="form-control"></select>
					</div>
				</div>

								<div class="col-md-4">
									<div class="input-group">
										<span class="input-group-addon">Y-Col</span>
											<select id="ycol_select" class="form-control"></select>
									</div>
								</div>

`;

function init_dialog(plot_id) {
	
	var plot_obj = plot_list[plot_id];
	var paras = plot_obj.plot_para;
	var col_names = plot_obj.col_names;

	if(paras[0]=="line_ploter"){
		$('#paras_div').html(line_para_html);
		
	}
	else if(paras[0]=="pie_ploter"){
		$('#paras_div').html(pie_para_html);
	}
	else if(paras[0]=="area_ploter"){
		$('#paras_div').html(line_para_html);
	}

	$("#plot_type_select").val(paras[0]);

	$("#title_input").val(paras[4]);
	$("#xtitle_input").val(paras[5]);
	$("#ytitle_input").val(paras[6]);

	var xcol_html = "";
	for(var i=0; i<col_names.length; i++){
		xcol_html += "<option>" + col_names[i] + "</option>";
	}
	$("#xcol_select").html(xcol_html);
	$("#ycol_select").html(xcol_html);
	$("#group_select").html(xcol_html);
	$("#xcol_select").val(col_names[paras[1]]);
	$("#ycol_select").val(col_names[paras[2]]);
	$("#group_select").val(col_names[paras[3]]);


	var table_html = "";
	for(var i in tableinfo){
		table_html += "<option>" + i + "</option>";
	}
	$("#table_dialog_select").html(table_html);

	$("#sql_edit").val(plot_obj.plot_sql);
}


function apply_dialog(plot_id){
	var plot_obj = plot_list[plot_id];
	var col_names = plot_obj.col_names;
	
	var plot_type = $("#plot_type_select").val();
	var title = $("#title_input").val();
	var xtitle = $("#xtitle_input").val();
	var ytitle = $("#ytitle_input").val();
	var xcol = col_names.indexOf($("#xcol_select").val());
	var ycol = col_names.indexOf($("#ycol_select").val());
	var gcol = col_names.indexOf($("#group_select").val());

	plot_obj.plot_para[0] = plot_type;
	plot_obj.plot_para[1] = xcol + "";
	plot_obj.plot_para[2] = ycol + "";
	plot_obj.plot_para[3] = gcol + "";
	plot_obj.plot_para[4] = title;
	plot_obj.plot_para[5] = xtitle;
	plot_obj.plot_para[6] = ytitle;

	var sql_cmd = $("#sql_edit").val();
	plot_obj.plot_sql = sql_cmd;
	plot_obj.get_data();

	plot_obj.plot_gra();
	
}

$(function(){

	$("#plot_dialog").on('hide.bs.modal', function(event){
		focus="plot_obj";
	});

	
	$('#plot_dialog').on('show.bs.modal', function (event) {

		var plot_id=0;
		for( var i in plot_list){
			if(plot_list[i].sta_select!=0){
				plot_id = i; break;
			}
		}
		var plot_obj=plot_list[plot_id];
		var paras = plot_obj.plot_para;
		var title = paras[4];
		
		var modal=$(this);
		modal.find('.modal-title').text(title)
		init_dialog(plot_id);

		$("#plot_apply_button").click(function(){
			var plot_id=0;
			for( var i in plot_list){
				if(plot_list[i].sta_select!=0){
					plot_id = i; break;
				}
			}
			apply_dialog(plot_id);
			init_dialog(plot_id);
		});

		$("#plot_type_select").change(function(){
			var plot_id=0;
			for( var i in plot_list){
				if(plot_list[i].sta_select!=0){
					plot_id = i; break;
				}
			}
			var plot_obj=plot_list[plot_id];
			plot_obj.plot_para[0] = $("#plot_type_select").val();
			init_dialog(plot_id);
		});

		$('#save_analyser_button').click(function(){
			var plot_obj = plot_list[plot_id];
			var col_names = plot_obj.col_names;
			
			var plot_type = $("#plot_type_select").val();
			var title = $("#title_input").val();
			var xtitle = $("#xtitle_input").val();
			var ytitle = $("#ytitle_input").val();
			var xcol = col_names.indexOf($("#xcol_select").val());
			var ycol = col_names.indexOf($("#ycol_select").val());
			var gcol = col_names.indexOf($("#group_select").val());

			var para = new Array();
			para[0] = plot_type;
			para[1] = xcol + "";
			para[2] = ycol + "";
			para[3] = gcol + "";
			para[4] = title;
			para[5] = xtitle;
			para[6] = ytitle;

			var plot_cmd = para.join(String.fromCharCode(1));
			var sql_cmd = $("#sql_edit").val();

			var table_name = $("#table_dialog_select").val();
			var analyser_name = $("#analyser_name_input").val();

			AJAX("cmd=save_analyser&analyser_name=" + analyser_name + "&table_name=" + table_name + "&plot_cmd=" + plot_cmd + "&sql_cmd=" + sql_cmd, function(){
				AJAX("cmd=analyserlist&table_name=" + cur_table_name, freshAnalyserList);				
			});


			
		});

		
	});
})
