package main

import (
	"database/sql"
	//	"encoding/json"
	"fmt"
	"crypto/md5"
	_ "github.com/go-sql-driver/mysql"
	//"os"
	"strings"
	"encoding/hex"
)

var DBInfo string = "payment:payment@tcp(127.0.0.1:3306)/"



func Query(cmd string) {
	db, err := sql.Open("mysql", DBInfo+"totoro_xtzhang_sparksql")
	defer db.Close()
	err = db.Ping()
	if err != nil {
		fmt.Println("Ping err")
		return
	}
	db.Query(cmd)
}



func CreateDB() {
	db, err := sql.Open("mysql", DBInfo)
	err = db.Ping()
	if err != nil {
		fmt.Println("Ping err")
		return
	}

	cmd := "DROP DATABASE totoro_xtzhang_sparksql"
	db.Query(cmd)
	cmd = "CREATE DATABASE totoro_xtzhang_sparksql"
	db.Query(cmd)
	db.Close()

	cmd = ` CREATE TABLE tableinfo (
					table_id INT NOT NULL AUTO_INCREMENT,
					table_name VARCHAR(1024) NOT NULL,
					file_name VARCHAR(1024) NOT NULL,
          col_name VARCHAR(1024) NOT NULL,
          col_type VARCHAR(1024) NOT NULL,
					description VARCHAR(1024),
					PRIMARY KEY(table_id),
					UNIQUE(table_id), UNIQUE(table_name)
	)`
	Query(cmd)
	cmd = ` CREATE TABLE analyser ( 
					analyser_id INT NOT NULL AUTO_INCREMENT,
					analyser_name VARCHAR(1024) NOT NULL,
					table_name VARCHAR(1024) NOT NULL,
					table_id INT NOT NULL,
					user_name VARCHAR(1024) NOT NULL,
					sql_cmd VARCHAR(1024) ,
					plot_cmd VARCHAR(1024) ,
          description VARCHAR(1024),
					PRIMARY KEY(analyser_id),
					UNIQUE(analyser_id), UNIQUE(analyser_name)
	)`
	Query(cmd)

	cmd = ` CREATE TABLE userinfo (
					user_id INT NOT NULL AUTO_INCREMENT,
					user_name VARCHAR(1024) NOT NULL,
          user_passwd VARCHAR(1024) NOT NULL,
          login_cookie VARCHAR(1024) NOT NULL,
					PRIMARY KEY(user_id),
					UNIQUE(user_id), UNIQUE(user_name)
	)`
	Query(cmd)
	
}

func main() {
	CreateDB()
	SP := string(rune(1))
	
	col_name := []string{"network_id", "start_date", "end_date", "budget_model", "placement_type", "price_model", "billing_method", "controlling_measuer"}
	col_name_str := strings.Join(col_name, string(rune(1)))
	col_type := []string{"INT", "STRING", "STRING", "STRING", "STRING", "STRING", "STRING", "STRING"}
	col_type_str := strings.Join(col_type, string(rune(1)))

	
	w := md5.New()
	w.Write([]byte("admin"))
	md5pwd := hex.EncodeToString(w.Sum(nil))
	cmd := "INSERT INTO userinfo (user_name, user_passwd, login_cookie) VALUES ('admin', '" + md5pwd + "', '')"
	Query(cmd)
	
	cmd = "INSERT INTO tableinfo (table_name, file_name, col_name, col_type, description) VALUES ('d_placement', 'd_placement', '" + col_name_str + "', '" + col_type_str + "', " +  "'placement table')"
	Query(cmd)

	pars := []string{"pie_ploter", "0", "1", "0", "Network_Proportion", "x title", "y title", "network proportion"}
	parstr := strings.Join(pars, SP)
	
	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, user_name, sql_cmd, plot_cmd, description) VALUES ('Network_Proportion','d_placement','1', 'admin', 'select network_id,count(*) from d_placement group by network_id', '" + parstr + "', '')"
	Query(cmd)

	pars = []string{"pie_ploter", "0", "1", "0", "Budget_Proportion", "x title", "y title", "budget proportion"}
	parstr = strings.Join(pars, SP)
	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, user_name, sql_cmd, plot_cmd, description) VALUES ('Budget_Model_Proportion','d_placement','1', 'admin', 'select budget_model,count(*) from d_placement group by budget_model', '" + parstr + "', '')"
	Query(cmd)

	pars = []string{"pie_ploter", "0", "1", "0", "Placement_Type_Proportion", "x title", "y title", "placement proportion"}
	parstr = strings.Join(pars, SP)
	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, user_name, sql_cmd, plot_cmd, description) VALUES ('Placement_Type_Proportion','d_placement','1', 'admin', 'select placement_type,count(*) from d_placement group by placement_type', '" + parstr + "', '')"
	Query(cmd)

	pars = []string{"pie_ploter", "0", "1", "0", "Price_Model_Proportion", "x title", "y title", "price model proportion"}
	parstr = strings.Join(pars, SP)
	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, user_name, sql_cmd, plot_cmd, description) VALUES ('Price_Model_Proportion','d_placement','1', 'admin', 'select price_model,count(*) from d_placement group by price_model', '" + parstr + "', '')"
	Query(cmd)

	pars = []string{"pie_ploter", "0", "1", "0", "Billing_Method_Proportion","x title", "y title",  "billing method proportion"}
	parstr = strings.Join(pars, SP)
	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, user_name, sql_cmd, plot_cmd, description) VALUES ('Billing_Method_Proportion','d_placement','1', 'admin', 'select billing_method,count(*) from d_placement group by billing_method', '" + parstr +"', '')"
	Query(cmd)

	pars = []string{"pie_ploter", "0", "1", "0", "Controlling_Measure_Proportion", "x title", "y title", "controlling measure proportion"}
	parstr = strings.Join(pars, SP)
	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, user_name, sql_cmd, plot_cmd, description) VALUES ('Controlling_Measure_Proportion','d_placement','1', 'admin', 'select controlling_measure,count(*) from d_placement group by controlling_measure', '" + parstr + "', '')"
	Query(cmd)

	pars = []string{"line_ploter", "2", "3","0", "Controlling_Measure_ByMonth","Month","Count", "controlling measure by month"}
	parstr = strings.Join(pars, SP)
	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, user_name, sql_cmd, plot_cmd, description) VALUES ('Controlling_Measure_ByMonth','d_placement','1', 'admin', 'select controlling_measure,year(start_date), month(start_date), count(*) from d_placement where year(start_date)=2016 group by controlling_measure,year(start_date),month(start_date)', '" + parstr + "', '')"
	Query(cmd)

	pars = []string{"area_ploter", "2", "3","0", "Billing_Method_ByMonth","Month","Count", "billing method by month"}
	parstr = strings.Join(pars, SP)
	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, user_name, sql_cmd, plot_cmd, description) VALUES ('Billing_Method_ByMonth','d_placement','1', 'admin', 'select billing_method,year(start_date), month(start_date), count(*) from d_placement where year(start_date)=2016 group by billing_method,year(start_date),month(start_date)', '" + parstr + "', '')"
	Query(cmd)

	pars = []string{"area_ploter", "2", "3","0", "Price_Model_ByMonth","Month","Count", "price model by month"}
	parstr = strings.Join(pars, SP)
	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, user_name, sql_cmd, plot_cmd, description) VALUES ('Price_Model_ByMonth','d_placement','1', 'admin', 'select price_model,year(start_date), month(start_date), count(*) from d_placement where year(start_date)=2016 group by price_model,year(start_date),month(start_date)', '" + parstr + "', '')"
	Query(cmd)

	pars = []string{"area_ploter", "2", "3","0", "Placement_Type_ByMonth","Month","Count", "placement type by month"}
	parstr = strings.Join(pars, SP)
	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, user_name, sql_cmd, plot_cmd, description) VALUES ('Placement_Type_ByMonth','d_placement','1', 'admin', 'select placement_type,year(start_date), month(start_date), count(*) from d_placement where year(start_date)=2016 group by placement_type,year(start_date),month(start_date)', '" + parstr + "','')"
	Query(cmd)

	pars = []string{"area_ploter", "2", "3","0", "Budget_Model_ByMonth","Month","Count", "budget model by month"}
	parstr = strings.Join(pars, SP)
	cmd = "INSERT INTO analyser (analyser_name, table_name, table_id, user_name, sql_cmd, plot_cmd, description) VALUES ('Budget_Model_ByMonth','d_placement','1', 'admin', 'select budget_model, year(start_date), month(start_date), count(*) from d_placement where year(start_date)=2016 group by budget_model,year(start_date),month(start_date)', '" + parstr + "', '')"
	Query(cmd)

}
