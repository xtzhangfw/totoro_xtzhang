package main

import (
	"encoding/json"
	"fmt"
	"time"
	"math/rand"
	"html/template"
	//	"io/ioutil"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	//"io"
	"net"
	"net/http"
	"strings"
	//	"os/exec"
	//	"strconv"
	//	"strings"
)

const (
	BLOCKSIZE int    = 100
	SPACE     string = string(rune(1))
	MAXLINES  int    = 10000
)

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

type TemplateData struct {
	Username string
	
}

var templatedata TemplateData

func DBQuery(cmd string) [][]string {
	db, err := sql.Open("mysql", "payment:payment@tcp(127.0.0.1:3306)/totoro_xtzhang_sparksql")
	defer db.Close()
	err = db.Ping()
	if err != nil {
		fmt.Println("Ping err")
		return nil
	}

	rows, _ := db.Query(cmd)
	defer rows.Close()
	cols, _ := rows.Columns()
	ln := len(cols)

	ansList := make([][]string, 0)

	for rows.Next() {
		tmp := make([]string, ln)
		tmpInterface := make([]interface{}, ln)
		for i := range tmp {
			tmpInterface[i] = &tmp[i]
		}
		rows.Scan(tmpInterface...)
		ansList = append(ansList, tmp)
	}

	return ansList
}

func SparkQuery(sql string) string {
	conn, err := net.Dial("tcp", ":9998")
	if err != nil {
		fmt.Println("connect failed")
		return ""
	}
	defer conn.Close()
	writeBlocks(conn, sql)
	return readBlocks(conn)

}

func writeBlocks(conn net.Conn, s string) {
	buf := make([]byte, 1)
	p, ls := 0, len(s)
	for p < ls {
		wl := min(ls-p, BLOCKSIZE)
		buf[0] = byte(wl)
		conn.Write(buf)
		conn.Write([]byte(s[p : p+wl]))
		p = p + wl
	}
	buf[0] = 0
	conn.Write(buf)
}

func readBlocks(conn net.Conn) string {
	ans := make([]byte, 0, 4096)
	for true {
		oneBuf := make([]byte, 1)
		_, err := conn.Read(oneBuf)
		num := int(oneBuf[0])
		if err != nil || num == 0 {
			break
		}
		for num > 0 {
			conn.Read(oneBuf)
			ans = append(ans, oneBuf[0])
			num -= 1
		}
	}
	return string(ans)
}

func str2json(s string, maxnum int) []byte {
	strList := strings.Split(s, "\n")
	if maxnum <= 0 {
		maxnum = len(strList)
	}
	maxnum = min(maxnum, len(strList))

	ans := make([][]string, 0, maxnum)
	for i := 0; i < maxnum; i++ {
		ans = append(ans, strings.Split(strList[i], SPACE))
	}
	ansJson, _ := json.Marshal(ans)
	return ansJson
}

func QueryHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		r.ParseForm()
		cmd := r.Form["cmd"][0]
		fmt.Println(cmd)

		if cmd=="sparksql" {
			sql_cmd := r.Form["sql_cmd"][0]
			ans := SparkQuery(sql_cmd)
			w.Write(str2json(ans, MAXLINES))
		} else if cmd=="tablelist" {
      ansJson,_ := json.Marshal(DBQuery("select * from tableinfo"))
			w.Write(ansJson)
		} else if cmd == "analyserlist" {
			flag, username := checkLogin(r)
			if flag {
				table_name := r.Form["table_name"][0]
				ansJson,_ := json.Marshal(DBQuery("select * from analyser where table_name='" + table_name + "' and (user_name='" + username +"' or user_name='0' )"))
				w.Write(ansJson)
			}
			
		} else if cmd == "logout"{
			username := r.Form["username"][0]
			DBQuery("update userinfo set login_cookie='" + "-1" + "' where user_name='" + username + "'");
			w.Write([]byte(""))
			
		}	else if cmd == "del_analyser"{
			flag, username := checkLogin(r)
			if flag {
				analyser_name := r.Form["analyser_name"][0]
				DBQuery("delete from analyser where user_name='" + username + "' and analyser_name='" + analyser_name + "'")
			}
			
		} else if cmd == "save_analyser" {
			flag, username := checkLogin(r)
			if flag {
				analyser_name := r.Form["analyser_name"][0]
				table_name := r.Form["table_name"][0]
				plot_cmd := r.Form["plot_cmd"][0]
				sql_cmd := r.Form["sql_cmd"][0]
				DBQuery("insert into analyser (analyser_name, table_name, table_id, user_name, sql_cmd, plot_cmd, description) VALUES ('" + analyser_name + "','" + table_name + "','" + "1" + "','" + username + "','" + sql_cmd + "','" + plot_cmd + "','')")
			}
			
		}	else {
			w.Write([]byte(""))
		}
	}
}

func LoginHandler(w http.ResponseWriter, r *http.Request){
	if r.Method == "POST" {
		r.ParseForm()
		username := r.Form["username"][0]
		pwd := r.Form["password"][0]
		qdata := DBQuery("select user_passwd from userinfo where user_name='" + username + "'")
		fmt.Println(username, pwd, qdata)
		
		if len(qdata)>0 && len(qdata[0])>0 && qdata[0][0]==pwd {
			rand.Seed(time.Now().Unix())
			cookie_str := fmt.Sprintf("%d", rand.Intn(1000000))
						
			cookie := http.Cookie{Name:"login_cookie", Value:cookie_str, Path:"/", MaxAge:86400}
			http.SetCookie(w, &cookie)
			cookie = http.Cookie{Name:"login_username", Value:username, Path:"/", MaxAge:86400}
			http.SetCookie(w, &cookie)
			
			DBQuery("update userinfo set login_cookie='" + cookie_str + "' where user_name='" + username + "'");
			
			t, err := template.ParseFiles("templates/index.html")
			if err != nil {
				fmt.Println("Parse Login err")
				return
			}
			templatedata.Username = username
			t.Execute(w, templatedata)
			return
		}
	}
	
	t, err := template.ParseFiles("templates/login.html")
	if err != nil {
		fmt.Println("Parse Login err")
		return
	}
	t.Execute(w, templatedata)
}

func checkLogin(r *http.Request) (bool, string){
	cookie1, err1 := r.Cookie("login_username")
	cookie2, err2 := r.Cookie("login_cookie")

	if err1==nil && err2==nil{
		cookie_username, login_cookie := cookie1.Value, cookie2.Value
		qdata := DBQuery("select login_cookie from userinfo where user_name='" + cookie_username + "'")
		if len(qdata)>0 && len(qdata[0])>0 && qdata[0][0]==login_cookie{
			return true,cookie_username
		}
	}
	return false,""
}

func IndexHandler(w http.ResponseWriter, r *http.Request) {
	cookie1, err1 := r.Cookie("login_username")
	cookie2, err2 := r.Cookie("login_cookie")

	if err1==nil && err2==nil{
		cookie_username, login_cookie := cookie1.Value, cookie2.Value
		qdata := DBQuery("select login_cookie from userinfo where user_name='" + cookie_username + "'")
		if len(qdata)>0 && len(qdata[0])>0 && qdata[0][0]==login_cookie{
			t, err := template.ParseFiles("templates/index.html")
			if err != nil {
				fmt.Println("Parse err")
				return
			}
			templatedata.Username = cookie_username
			t.Execute(w, templatedata)
			return
		}
	}

	t, err := template.ParseFiles("templates/login.html")
	if err != nil {
		fmt.Println("Parse err")
		return
	}
	t.Execute(w, templatedata)
	return

}

func main() {
	http.HandleFunc("/", IndexHandler)
	http.HandleFunc("/Query", QueryHandler)
	http.HandleFunc("/Login", LoginHandler)
	fs := http.FileServer(http.Dir("/home/zxt/IdeaProjects/totoro_xtzhang/SparkSQLVersion/static/"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))
	http.ListenAndServe(":8887", nil)
}
