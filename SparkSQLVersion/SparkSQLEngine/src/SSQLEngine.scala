
import java.io.{BufferedInputStream, BufferedOutputStream, File, PrintStream}
import java.net.ServerSocket
import java.util

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.SparkConf
import org.apache.hadoop.fs._
import org.apache.spark
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema
import org.apache.spark.sql.{DataFrame, Row, SQLContext, SparkSession}
import org.apache.spark.sql.types.{IntegerType, StringType, StructField, StructType}
import org.apache.spark.sql.functions.udf

import scala.collection.mutable
import scala.io.BufferedSource
import scala.math._


object Cst{
  val SPACE=Character.toString(1.toChar)
}


object SparkSQLEngine {

  val tablePath="hdfs://localhost:9000/Tables"
  val tableList = getTableList()
  val tableInfo = mutable.Map[String, Array[(String,String)]]()
  for(tb <- tableList){
    tableInfo += ( tb -> getTableInfo(tb))
  }

  val spark = SparkSession
    .builder()
    .appName("Spark SQL basic example")
    .config("spark.some.config.option", "some-value")
    .getOrCreate()

  initRDD()

  def getTableList():Array[String]={
    val conf = new SparkConf().setAppName("GetTableList")
    val sc = new SparkContext(conf)
    val fList = FileSystem.get( sc.hadoopConfiguration ).globStatus( new Path(tablePath + "/" + "*.info"))

    val ans=new Array[String](fList.length)
    for(i <- 0 until fList.length){
      ans(i) = fList(i).getPath.getName
      val ln = ans(i).length
      ans(i) = ans(i).substring(0, ln-5)
    }
    sc.stop()
    ans
  }

  def getTableInfo(tname: String):(Array[(String,String)])={
    val conf = new SparkConf().setAppName("GetTableInfo " + tname)
    val sc = new SparkContext(conf)
    val rdd = sc.textFile(tablePath + "/" + tname + ".info")
    val lines = rdd.map(l=>l.split(Character.toString(1.toChar))).collect()

    sc.stop()
    lines(0).zip(lines(1))
  }

  def initRDD(): Unit ={

    for(i <- 0 until tableList.length){
      val info = tableInfo(tableList(i))

      val fields = info.map(item=>{
        val col_name = item._1;
        val col_type = item._2;
        StructField(col_name, StringType, nullable = false)
      })
      val schema = StructType(fields)
      val rdd = spark.sparkContext.textFile(tablePath + "/" + tableList(i))
        .map(l=>l.split(Character.toString(1.toChar)))
        .map(line=>{Row(line:_*)})

      val df = spark.createDataFrame(rdd, schema)

      val toInt = udf[Int, String](_.toInt)
      val toFloat = udf[Float, String](_.toFloat)
      val toDouble = udf[Double, String](_.toDouble)

      def format_df(df_cur:DataFrame, i: Int): DataFrame ={
        if(i==info.length) return df_cur
        val col_name = info(i)._1
        val col_type = info(i)._2.toUpperCase
        if(col_type=="INT") {
          format_df(df_cur.withColumn(col_name, toInt(df(col_name))), i+1 )
        }
        else if(col_type=="FLOAT"){
          format_df(df_cur.withColumn(col_name, toFloat(df(col_name))), i+1)
        }
        else if(col_type=="DOUBLE"){
          format_df(df_cur.withColumn(col_name, toDouble(df(col_name))), i+1)
        }
        else{
          format_df(df_cur, i+1)
        }
      }

      val df_new = format_df(df, 0)

      df_new.createOrReplaceTempView(tableList(i))
    }

    //spark.sql("select year(start_date),sum(network_id) from d_placement group by start_date").show()
  }


  def query(sql: String): List[org.apache.spark.sql.Row] ={
    val sql_res = spark.sql(sql)
    val col_names = sql_res.columns;
    val ans = sql_res.collect().toList
    return Row.fromSeq(col_names) :: ans
  }


  def main(args: Array[String]) {
    //json2parquet()
    val server = new ServerSocket(9998)
    while (true) {
      val s = server.accept()
      s.setSoTimeout(60000)
      val in = new BufferedInputStream(s.getInputStream)
      val out = new BufferedOutputStream(s.getOutputStream())
      try {

        val BLOCKSIZE = 100

        def readOneBlock(leftNum:Int, cur:String):String ={
          if(leftNum==0) return cur
          val c = in.read()
          readOneBlock(leftNum-1, cur + c.toChar.toString)
        }

        def readBlocks(cur:String):String = {
          val num = in.read()
          if(num<=0) return cur
          readBlocks(cur + readOneBlock(num, ""))
        }

        val sql = readBlocks("")
        println("Query:" + sql)
        val ans = query(sql).map(f=>f.mkString(Cst.SPACE)).mkString("\n")
        println("Done")

        def writeOneBlock(point:Int, s:String):Unit={
          if(point<s.length){
            out.write(s(point))
            writeOneBlock(point+1,s)
          }
        }

        def writeBlocks(point:Int, s:String): Unit ={
          val ls = s.length
          val wl = min(ls-point, BLOCKSIZE)
          if(wl<=0){ out.write(0); out.flush() }
          else{
            out.write(wl)
            writeOneBlock(0, s.substring(point, point+wl))
            writeBlocks(point+wl, s)
          }
        }
        writeBlocks(0, ans)
        s.close()
        in.close()
        out.close()

      }catch{
        case err: Any =>{
          println("Error" + err.toString)
          out.write(0);
          out.flush();
          if(!s.isClosed()) s.close()
          //in.close(); out.close()

        }
      }
    }
  }

}
