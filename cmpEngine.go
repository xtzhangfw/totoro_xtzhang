package main

import (
	"fmt"
	"net"
	"time"
)

const (
	BLOCKSIZE int    = 100
	SPACE     string = string(rune(1))
	MAXLINES  int    = 10000
)

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func SparkQueryHiveSQL(sql string) string {
	conn, err := net.Dial("tcp", ":9997")
	if err != nil {
		fmt.Println("connect failed")
		return ""
	}
	defer conn.Close()
	writeBlocks(conn, sql)
	return readBlocks(conn)

}



func SparkQueryTSQL(sql string) string {
	conn, err := net.Dial("tcp", ":9999")
	if err != nil {
		fmt.Println("connect failed")
		return ""
	}
	defer conn.Close()
	writeBlocks(conn, sql)
	return readBlocks(conn)

}

func SparkQuerySparkSQL(sql string) string {
	conn, err := net.Dial("tcp", ":9998")
	if err != nil {
		fmt.Println("connect failed")
		return ""
	}
	defer conn.Close()
	writeBlocks(conn, sql)
	return readBlocks(conn)

}



func writeBlocks(conn net.Conn, s string) {
	buf := make([]byte, 1)
	p, ls := 0, len(s)
	for p < ls {
		wl := min(ls-p, BLOCKSIZE)
		buf[0] = byte(wl)
		conn.Write(buf)
		conn.Write([]byte(s[p : p+wl]))
		p = p + wl
	}
	buf[0] = 0
	conn.Write(buf)
}

func readBlocks(conn net.Conn) string {
	ans := make([]byte, 0, 4096)
	for true {
		oneBuf := make([]byte, 1)
		_, err := conn.Read(oneBuf)
		num := int(oneBuf[0])
		if err != nil || num == 0 {
			break
		}
		for num > 0 {
			conn.Read(oneBuf)
			ans = append(ans, oneBuf[0])
			num -= 1
		}
	}
	return string(ans)
}

func main() {
  t1 := time.Now()
  for i:=0; i<100; i++{
    sqlSparkSQL := "select budget_model, year(start_date), month(start_date), count(*) from d_placement where year(start_date)=2016 group by budget_model,year(start_date),month(start_date)"
    SparkQuerySparkSQL(sqlSparkSQL)
    if i%10==9 {
      fmt.Println("SparkSQL: ", time.Since(t1))
    }
  }

  t2 := time.Now()
  for i:=0; i<100; i++{
    sqlTSQL := "select budget_model, year(start_date), month(start_date), sum(1) from d_placement where year(start_date)=2016 group budget_model,year(start_date),month(start_date)"
    SparkQueryTSQL(sqlTSQL)
    if i%10==9 {
      fmt.Println("TSQL: ", time.Since(t2))
    }
  }

  t3 := time.Now()
  for i:=0; i<100; i++{
    sqlHiveSQL := "select budget_model, year(start_date), month(start_date), count(*) from d_placement where year(start_date)=2016 group by budget_model,year(start_date),month(start_date)"
    SparkQueryHiveSQL(sqlHiveSQL)
    if i%10==9 {
      fmt.Println("HiveSQL: ", time.Since(t3))
    }
  }


}
